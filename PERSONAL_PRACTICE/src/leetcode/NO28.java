package leetcode;

public class NO28 {

    public static void main(String[] args) {
        System.out.println(strStr("bbaa", "aab"));

    }

    // 1-----------------------------------------------------------------------
    // beat 1%
    public static int strStr(String haystack, String needle) {
        char[] hCharAry = haystack.toCharArray();
        char[] nCharAry = needle.toCharArray();
        int hLen = haystack.length();
        int nLen = needle.length();
        if (nLen == 0) {
            return 0;
        }
        if (hLen == 0 || nLen > hLen) {
            return -1;
        }
        outer: for (int i = 0; i < hLen; i++) {
            inner: for (int j = 0; j < nLen; j++) {
                if (i + j >= hLen || hCharAry[i + j] != nCharAry[j]) {
                    continue outer;
                }
            }
            return i;
        }
        return -1;
    }

}
