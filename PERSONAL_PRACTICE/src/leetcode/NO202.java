package leetcode;

//ok
public class NO202 {

    public static void main(String[] args) {
        System.out.println(Integer.MAX_VALUE);
        System.out.println(digitSquareSum2(5435));
        System.out.println(digitSquareSum(5435));
        // 2147483647

    }

    public static boolean isHappy(int n) {
        boolean[] ary = new boolean[730];
        while (true) {
            int sum = digitSquareSum(n);
            if (sum == 1) {
                return true;
            } else {
                if (ary[sum]) {
                    return false;
                } else {
                    ary[sum] = true;
                    n = sum;
                }
            }
        }
    }

    public static int digitSquareSum2(int n) {
        // ***[int] int to string
        String str = Integer.toString(n);
        char[] ary = str.toCharArray();
        int sum = 0;
        for (char c : ary) {
            // ***[char] char to string
            String s = Character.toString(c);
            int i = Integer.parseInt(s);
            sum += (i * i);
        }
        return sum;
    }

    public static int digitSquareSum(int n) {
        int sum = 0;
        int currDigt;
        // ***[int] 數字一次取一個位數，可用%
        while (n != 0) {
            currDigt = n % 10;
            n = n / 10;
            sum += (currDigt * currDigt);
        }
        return sum;
    }

}
