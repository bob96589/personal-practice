package leetcode;

//ok
public class NO168 {

    public static void main(String[] args) {

        // System.out.println((char)('A' + 1));

        System.out.println(convertToTitle2(1));
        System.out.println(convertToTitle2(26));
        System.out.println(convertToTitle2(27));
        System.out.println(convertToTitle2(28));
        System.out.println(convertToTitle2(29));
        System.out.println(convertToTitle2(52));// aZ
        System.out.println("------");
        System.out.println(convertToTitle(1));
        System.out.println(convertToTitle(26));
        System.out.println(convertToTitle(27));
        System.out.println(convertToTitle(28));
        System.out.println(convertToTitle(29));
        System.out.println(convertToTitle(52));// aZ

    }

    public static String convertToTitle2(int n) {
        StringBuilder sb = new StringBuilder();
        int num = n - 1;
        int remainder;
        while (num >= 0) {
            remainder = num % 26;
            // ***[char] calculate
            sb.insert(0, (char) ('A' + remainder));
            num = (num - remainder) / 26 - 1;
        }
        return sb.toString();
    }

    public static String convertToTitle(int n) {
        String[] charAry = new String[] { "Z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y" };
        StringBuffer sb = new StringBuffer();
        int num = n;
        while (num != 0) {
            int remainder = num % 26;
            // ***[stringbuffer] insert
            sb.insert(0, charAry[remainder]);
            num = (int) Math.floor(num / 26);
            if (remainder == 0) {
                num--;
            }

        }
        return sb.toString();
    }
}
