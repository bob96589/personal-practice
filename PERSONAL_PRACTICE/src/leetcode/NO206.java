package leetcode;

//ok
public class NO206 {

    public static void main(String[] args) {

    }

    public static ListNode reverseList2(ListNode head) {
        ListNode prev = null;
        ListNode current = head;
        if (current == null) {
            return prev;
        }
        ListNode next = current.next;
        while (true) {
            current.next = prev;
            prev = current;
            current = next;
            if (current == null) {
                break;
            }
            next = current.next;
        }
        return prev;
    }

    public static ListNode reverseList(ListNode head) {
        ListNode prev = null;
        ListNode current = head;
        while (current != null) {
            ListNode next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return prev;
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }
}
