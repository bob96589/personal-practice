package leetcode;

public class NO275 {

    public static void main(String[] args) {
        System.out.println(hIndex(new int[] { 1, 4, 7, 9 })); // 3
        System.out.println(hIndex(new int[] { 1 })); // 1

    }

    // beat 9%
    public static int hIndex(int[] citations) {
        int len = citations.length;
        int leftIndex = 0;
        int rightIndex = len - 1;

        int result = 0;
        while (leftIndex <= rightIndex) {
            int mid = (leftIndex + rightIndex) / 2;
            int paperCnt = len - mid;
            int citationsCnt = citations[mid];
            if (paperCnt == citationsCnt) {
                return paperCnt;
            } else if (paperCnt > citationsCnt) {
                leftIndex = mid + 1;
                result = Math.max(result, citationsCnt);
            } else {// paperCnt < citationsCnt
                rightIndex = mid - 1;
                result = Math.max(result, paperCnt);
            }
        }
        return result;
    }

}
