package java;

public class ThreadA {
    public static void main(String args[]) {
        System.out.println("Thread A 執行");

        Thread threadB = new Thread(new Runnable() {
            public void run() {
                try {
                    System.out.println("Thread B 開始");
                    for (int i = 0; i < 5; i++) {
                        Thread.sleep(1000);
                        System.out.println("Thread B 執行.......");
                    }
                    System.out.println("Thread B 即將結束");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        threadB.start();

        try {
            threadB.join(); // 使用 join()
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // threadB 透過 join() 加入，因此必須等 threadB 執行完後才會執行此行
        // 若沒用 join()，很快就會執行到這邊
        System.out.println("Thread A 執行");
    }
}