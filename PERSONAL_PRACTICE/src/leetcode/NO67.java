package leetcode;

//TODO
public class NO67 {

    public static void main(String[] args) {
        // System.out.println(addBinary("10100000100100110110010000010101111011011001101110111111111101000000101111001110001111100001101",
        // "110101001011101110001111100110001010100001101011101010000011011011001011101111001100000011011110011"));
        // System.out.println(addBinary3("11", "1"));

        // ***[char] char calculate
        // System.out.println('z'-'a');

        // ***[char]declare char
        // char c = '7';
        // ***[char] convert char to int
        // System.out.println(Character.getNumericValue(c));

        // ***[string] StringBuffer insert(0, str) == prepend
        StringBuffer a = new StringBuffer();
        a.append("aaaaaa");
        a.insert(0, "s");
        a.append("z");
        System.out.println(a.toString());
    }

    // ***[string] binary相加，此方法會爆炸
    private static String addBinary(String a, String b) {
        Long a_10 = Long.parseLong(a, 2);
        Long b_10 = Long.parseLong(b, 2);
        Long result_10 = a_10 + b_10;
        String result_2 = Long.toBinaryString(result_10);
        // 2進位: Binary, Integer.toOctalString(i)
        // 8進位: Octal, Integer.toOctalString(i)
        // 10進位: Decimal,
        // 16進位: Hexadecimal, Integer.toHexString(i)
        return result_2;
    }

    private static String addBinary2(String a, String b) {
        // string to char array
        char[] aAry = a.toCharArray();
        char[] bAry = b.toCharArray();

        int aIndex = a.length() - 1;
        int bIndex = b.length() - 1;
        int additionPoint = 0;
        StringBuffer sb = new StringBuffer();
        while (aIndex >= 0 || bIndex >= 0) {
            boolean aDone = aIndex < 0;
            boolean bDone = bIndex < 0;
            // ***[char]char to int
            int aValue = aDone ? 0 : Character.getNumericValue(aAry[aIndex]);
            int bValue = bDone ? 0 : Character.getNumericValue(bAry[bIndex]);
            int sum = additionPoint + aValue + bValue;
            additionPoint = sum / 2;
            sb.append(sum % 2);
            if (!aDone) {
                aIndex--;
            }
            if (!bDone) {
                bIndex--;
            }
        }

        if (additionPoint != 0) {
            sb.append(additionPoint);
        }
        // ***[string]stringbuffer reverse
        return sb.reverse().toString();
    }

    private static String addBinary3(String a, String b) {
        StringBuilder sb = new StringBuilder();
        int i = a.length() - 1, j = b.length() - 1, carry = 0;
        while (i >= 0 || j >= 0) {
            int sum = carry;
            if (j >= 0)
                // ***[string]char String.charAt(int index)
                // ***[string]char - '0' >> int
                sum += b.charAt(j--) - '0';
            if (i >= 0)
                sum += a.charAt(i--) - '0';
            sb.append(sum % 2);
            carry = sum / 2;
        }
        if (carry != 0)
            sb.append(carry);
        return sb.reverse().toString();
    }

}
