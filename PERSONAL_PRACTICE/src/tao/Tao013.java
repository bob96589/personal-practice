package tao;

import java.util.HashSet;
import java.util.Set;

public class Tao013 {
    public static void main(String[] args) {
        int[] nums = new int[] { 11, 22, 33, 44, 11, 13, 11, 22 };
        Set<Integer> set = new HashSet<Integer>();
        for (int i = 0; i < nums.length; i++) {
            set.add(nums[i]);
        }
        System.out.println(set.size());
    }
}
