package leetcode;

//TODO
public class NO91 {

    public static void main(String[] args) {
        System.out.println(numDecodings("0"));
        System.out.println(numDecodings2("0"));

    }

    public static int numDecodings(String s) {
        if (s.isEmpty()) {
            return 0;
        }
        if (s.startsWith("0")) {
            return 0;
        }

        int preConnect = 0;
        int preSeperate = 1;
        int preSum = 1;

        int connect = 0;
        int seperate = 0;
        int sum = 0;

        int lastIndex = s.length() - 1;

        for (int i = 0; i < lastIndex; i++) {
            StringBuffer sb = new StringBuffer();
            sb.append(s.charAt(i));
            sb.append(s.charAt(i + 1));
            if (checkIfConnectable(sb.toString())) {
                connect = preSeperate;
            } else {
                connect = 0;
            }
            if ('0' != s.charAt(i + 1)) {
                seperate = preSum;
            } else {
                seperate = 0;
            }
            sum = connect + seperate;

            preConnect = connect;
            preSeperate = seperate;
            preSum = sum;
        }

        return preSum;

    }

    private static boolean checkIfConnectable(String str) {
        if (str.startsWith("0")) {
            return false;
        }
        return 1 <= Integer.parseInt(str) && Integer.parseInt(str) <= 26;
    }

    public static int numDecodings2(String s) {
        if (s.isEmpty()) {
            return 0;
        }
        if (s.startsWith("0")) {
            return 0;
        }

        int len = s.length();

        // ***[array] declare int array
        int[] memo = new int[len + 1];
        memo[0] = 1;
        // ***[string] get specific char in string
        memo[1] = s.substring(0, 1) != "0" ? 1 : 0;

        for (int i = 2; i <= len; i++) {
            if (checkIfConnectable(s.substring(i - 2, i))) {
                memo[i] += memo[i - 2];
            }
            if (!"0".equals(s.substring(i - 1, i))) {
                memo[i] += memo[i - 1];
            }
        }

        return memo[len];

    }

}
