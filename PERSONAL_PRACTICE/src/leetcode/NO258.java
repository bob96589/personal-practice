package leetcode;

//TODO
public class NO258 {

    public static void main(String[] args) {

    }

    public static int addDigits3(int num) {
        return (num - 1) % 9 + 1;
    }

    public static int addDigits2(int num) {
        return (num % 9 == 0 && num != 0) ? 9 : num % 9;
    }

    public static int addDigits(int num) {
        while (true) {
            int sumOfDigit = getSumOfDigits(num);
            if (sumOfDigit < 10) {
                return sumOfDigit;
            } else {
                num = sumOfDigit;
            }
        }
    }

    public static int getSumOfDigits(int num) {
        int sumOfDigit = 0;
        int currDigit;
        while (num != 0) {
            currDigit = num % 10;
            num /= 10;
            sumOfDigit += currDigit;
        }
        return sumOfDigit;
    }

}
