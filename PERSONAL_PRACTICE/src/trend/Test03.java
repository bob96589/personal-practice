package trend;

import java.util.ArrayList;
import java.util.List;

public class Test03 {

    // �䤤��48��57����0��9�Q�Ӫ��ԧB�Ʀr
    //
    // 65��90����26�Ӥj�g�^��r��
    // 97��122����26�Ӥp�g�^��r��

    public static void main(String[] args) {
        // System.out.println(getCharList("4AB"));
        // System.out.println(getCharList("A4B"));
        // System.out.println(getCharList("AB4"));
        //
        // System.out.println(getCharList("10AB"));
        // System.out.println(getCharList("A10B"));
        // System.out.println(getCharList("AB10"));

        System.out.println(solution("A2Le", "2pL1"));// t
        System.out.println(solution("a10", "10a"));// t
        System.out.println(solution("ba1", "1Ad"));// f
        System.out.println(solution("3x2x", "8"));// f
    }

    public static boolean solution(String S, String T) {
        List<Character> listS = getCharList(S);
        List<Character> listT = getCharList(T);
        if (listS.size() != listT.size()) {
            return false;
        }
        for (int i = 0; i < listS.size(); i++) {
            if (listS.get(i).equals('@') || listT.get(i).equals('@')) {
                continue;
            }
            if (!listS.get(i).equals(listT.get(i))) {
                return false;
            }
        }
        return true;
    }

    public static List<Character> getCharList(String str) {
        char[] charAry = str.toCharArray();
        int len = charAry.length;
        List<Character> list = new ArrayList<Character>();
        for (int i = 0; i < len; i++) {
            if (48 <= charAry[i] && charAry[i] <= 57) { // 0-9
                StringBuffer sb = new StringBuffer();
                sb.append(charAry[i]);
                for (int j = i + 1; j <= len; j++) {
                    if (j == len) {
                        int num = Integer.parseInt(sb.toString());
                        for (int k = 0; k < num; k++) {
                            list.add('@');
                        }
                    } else {
                        if (48 <= charAry[j] && charAry[j] <= 57) {
                            sb.append(charAry[j]);
                        } else {
                            int num = Integer.parseInt(sb.toString());
                            for (int k = 0; k < num; k++) {
                                list.add('@');
                            }
                            i = j - 1;
                            break;
                        }
                    }
                }

            } else {// a-z A-Z
                list.add(charAry[i]);
            }
        }
        return list;
    }

}
