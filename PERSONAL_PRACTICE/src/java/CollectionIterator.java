package java;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class CollectionIterator {

    public static void main(String[] args) {
        arrayListSet();
    }

    public static void foreach(Collection<String> collection) {
        Iterator<String> iterator = collection.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    public static void arrayListSet() {
        List<String> list = new ArrayList<String>();
        list.add("0");
        list.add("1");
        list.add("2");
        list.add("3");
        System.out.println(list);
        // ***[list] set�����л\��index����
        list.set(2, "8");
        System.out.println(list);
    }

}
