package leetcode;

import java.util.Arrays;
import java.util.Comparator;

public class NO179 {

    public static void main(String[] args) {
        // Integer[] nums = new Integer[]{4,3,6,2,8,9};
        // int[] nums = new int[] { 87,88, 8, };// 80,89,8
        // int[] nums = new int[] { 4,3,6,2,8,9 };//
        int[] nums = new int[] { 1, 2 };//
        // 89,88,8,878,87,80
        // 87,878 *878(87),87
        // 121,12 *12(1),121(1)
        System.out.println(largestNumber2(new int[] { 1, 2 }));
        System.out.println(largestNumber2(new int[] { 4, 3, 6, 2, 8, 9 }));
        System.out.println(largestNumber2(new int[] { 87, 88, 8 }));
        System.out.println(largestNumber2(new int[] { 80, 89, 8 }));

    }

    public static String largestNumber2(int[] nums) {
        Integer[] intAry = new Integer[nums.length];
        for (int i = 0; i < nums.length; i++) {
            intAry[i] = nums[i];
        }
        Arrays.sort(intAry, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                String str1 = Integer.toString(o1);
                String str2 = Integer.toString(o2);
                long a = Long.parseLong(str1 + str2);
                long b = Long.parseLong(str2 + str1);
                return a > b ? -1 : 1;
            }
        });
        StringBuffer sb = new StringBuffer();
        boolean first = true;
        for (int num : intAry) {
            if (first) {
                if (num == 0) {
                    return "0";
                }
                first = false;
            }
            sb.append(num);
        }
        return sb.toString();
    }

    public static String largestNumber(int[] nums) {

        Integer[] intAry = new Integer[nums.length];
        for (int i = 0; i < nums.length; i++) {
            intAry[i] = nums[i];
        }

        Arrays.sort(intAry, new Comparator<Integer>() {
            @Override
            public int compare(Integer num1, Integer num2) {
                // TODO return 1 if rhs should be before lhs
                // return -1 if lhs should be before rhs
                // return 0 otherwise
                String str1 = Integer.toString(num1);
                String str2 = Integer.toString(num2);

                int len1 = str1.length();
                int len2 = str2.length();

                String fisrtDigit1 = str1.substring(0, 1);
                String fisrtDigit2 = str2.substring(0, 1);
                int fisrtDigitNum1 = Integer.parseInt(fisrtDigit1);
                int fisrtDigitNum2 = Integer.parseInt(fisrtDigit2);

                if (len1 == len2) {
                    int val1 = Integer.parseInt(str1);
                    int val2 = Integer.parseInt(str2);
                    System.out.println(val1 + " vs " + val2 + ": " + (val1 > val2 ? val1 : val2));
                    return val1 >= val2 ? -1 : 1;
                } else if (len1 > len2) {
                    int distance = len1 - len2;
                    int val1 = Integer.parseInt(str1);
                    String m2 = str2;
                    for (int i = 0; i < distance; i++) {
                        m2 += fisrtDigit2;
                    }
                    int val2 = Integer.parseInt(m2);
                    System.out.println(val1 + " vs " + str2 + ": " + (val1 >= val2 ? val1 : str2));
                    return val1 >= val2 ? -1 : 1;
                } else {// len1 < len2
                    int distance = len2 - len1;
                    int val2 = Integer.parseInt(str2);
                    String m1 = str1;
                    for (int i = 0; i < distance; i++) {
                        m1 += fisrtDigit1;
                    }
                    int val1 = Integer.parseInt(m1);
                    System.out.println(str1 + " vs " + val2 + ": " + (val1 >= val2 ? val2 : str1));
                    return val1 > val2 ? -1 : 1;
                }
            }
        });
        StringBuffer sb = new StringBuffer();
        for (int num : intAry) {
            sb.append(num);
        }
        return sb.toString();

    }

}
