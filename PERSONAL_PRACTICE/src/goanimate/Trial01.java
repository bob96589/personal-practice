package goanimate;

import java.util.Arrays;
import java.util.Scanner;

public class Trial01 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
    }

    static String findNumber(int[] arr, int k) {
        Arrays.sort(arr);
        int result = Arrays.binarySearch(arr, k);
        return result >= 0 ? "YES" : "NO";
    }

}
