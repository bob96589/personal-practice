package leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

// ok
// important
public class NO104 {

    public static void main(String[] args) {
        TreeNode t1 = new TreeNode(1);
        TreeNode t2 = new TreeNode(2);
        TreeNode t3 = new TreeNode(3);
        t1.right = t2;
        t1.right.right = t3;
        // System.out.println(maxDepth(t1));
        // System.out.println(maxDepth(null));
        // System.out.println(maxDepth_dfs(t1));
        System.out.println(maxDepth(t1));

    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }

        @Override
        public String toString() {
            return Integer.toString(val);
        }

    }

    // 4--------------------------------------------------------------------------------------
    // ***[dfs]回傳中間值
    private static int maxDepth(TreeNode root) {// beat 18%
        if (root == null) {
            return 0;
        }
        int leftHeight = maxDepth(root.left);
        int rightHeight = maxDepth(root.right);
        return Math.max(leftHeight, rightHeight) + 1;
    }

    // 3--------------------------------------------------------------------------------------
    // ***[dfs]僅有陣列、list才能取到DFS中間的值
    // 非常不好的方法
    private static void dfs2(TreeNode root, int[] maxDepth, int depth) {// beat
                                                                        // 18%
        if (root.left == null && root.right == null) {// leaf
            maxDepth[0] = Math.max(maxDepth[0], depth);
        }
        if (root.left != null) {
            dfs2(root.left, maxDepth, depth + 1);
        }
        if (root.right != null) {
            dfs2(root.right, maxDepth, depth + 1);
        }
    }

    public static int maxDepth_dfs2(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int[] maxDepth = new int[] { 1 };
        dfs2(root, maxDepth, 1);
        return maxDepth[0];
    }

    // 2--------------------------------------------------------------------------------------
    public static int maxDepth_bfs(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        if (root == null) {
            return 0;
        }
        queue.add(root);
        int level = 0;
        while (!queue.isEmpty()) {
            level++;
            int len = queue.size();
            while (len > 0) {
                len--;
                TreeNode n = (TreeNode) queue.remove();
                if (n.left != null) {
                    queue.add(n.left);
                }
                if (n.right != null) {
                    queue.add(n.right);
                }
            }
        }
        return level;
    }

    // 1--------------------------------------------------------------------------------------
    public static int maxDepth6(TreeNode root) {// beat 3%
        if (root == null) {
            return 0;
        }
        List<TreeNode> currentList = new ArrayList<TreeNode>();
        List<TreeNode> nextList = new ArrayList<TreeNode>();
        currentList.add(root);
        int i = 0;
        while (!currentList.isEmpty()) {
            i++;
            for (TreeNode treeNode : currentList) {
                if (treeNode.left != null) {
                    nextList.add(treeNode.left);
                }
                if (treeNode.right != null) {
                    nextList.add(treeNode.right);
                }
            }
            currentList.clear();
            currentList.addAll(nextList);
            nextList.clear();
        }
        return i;
    }

}
