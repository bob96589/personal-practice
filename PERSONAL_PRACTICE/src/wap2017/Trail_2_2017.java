package wap2017;

import java.util.Arrays;
import java.util.Scanner;

public class Trail_2_2017 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int matrixSize = in.nextInt();
        int[][] data = new int[matrixSize][matrixSize];

        int result = 0;
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                data[i][j] = in.nextInt();
                result += data[i][j];
            }
        }

        System.out.println(result);
        // printTwoDimensionalAry(data);

    }

    public static void printTwoDimensionalAry(int[][] memo) {
        for (int[] ary : memo) {
            System.out.println(Arrays.toString(ary));
        }
    }

}
