package leetcode;

public class NO191 {

    public static void main(String[] args) {
        System.out.println(hammingWeight(Integer.MAX_VALUE));

    }

    public static int hammingWeight(int n) {
        int cnt = 0;
        while (n != 0) {
            cnt += (n & 1);
            n >>>= 1;
        }
        return cnt;
    }

}
