package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

//important
public class NO417 {

    public static void main(String[] args) {

        // int[][] matrix = new int[][] { { 1, 2, 2, 3, 5 }, { 3, 2, 3, 4, 4 },
        // { 2, 4, 5, 3, 1 }, { 6, 7, 1, 4, 5 }, { 5, 1, 1, 2, 4 } };
        // int[][] matrix = new int[][] {{1,2,3},{8,9,4},{7,6,5}};
        int[][] matrix = new int[][] { { 3, 3, 3 }, { 3, 1, 3 }, { 0, 2, 4 } };
        List<int[]> list = pacificAtlantic2(matrix);
        for (int[] a : list) {
            System.out.println(Arrays.toString(a));
        }

    }

    // 2---------------------------------------------------------------------------------------------
    // ***[dfs] matrix
    public static List<int[]> pacificAtlantic2(int[][] matrix) {
        List<int[]> resultList = new ArrayList<int[]>();
        int iLen = matrix.length;
        if (iLen == 0) {
            return resultList;
        }
        int jLen = matrix[0].length;
        // ------------------------
        boolean[][] pacificVisited = new boolean[iLen][jLen];
        boolean[][] atlanticVisited = new boolean[iLen][jLen];
        Queue<int[]> pacificQueue = new LinkedList<int[]>();
        Queue<int[]> atlanticQueue = new LinkedList<int[]>();
        for (int i = 0; i < iLen; i++) {
            pacificQueue.add(new int[] { i, 0 });
            atlanticQueue.add(new int[] { i, jLen - 1 });
        }
        for (int j = 0; j < jLen; j++) {
            pacificQueue.add(new int[] { 0, j });
            atlanticQueue.add(new int[] { iLen - 1, j });
        }

        // fill pacificVisited, atlanticVisited
        for (int[] startPos : pacificQueue) {
            dfs(matrix, pacificVisited, startPos);
        }
        for (int[] startPos : atlanticQueue) {
            dfs(matrix, atlanticVisited, startPos);
        }
        // print pacificVisited, atlanticVisited
        for (boolean[] a : pacificVisited) {
            System.out.println(Arrays.toString(a));
        }
        // compare result
        for (int i = 0; i < iLen; i++) {
            for (int j = 0; j < jLen; j++) {
                if (pacificVisited[i][j] && atlanticVisited[i][j]) {
                    resultList.add(new int[] { i, j });
                }
            }
        }
        return resultList;
    }

    // ***[matrix]將方向定義成屬性
    final static int[][] directions = new int[][] { { -1, 0 }, { 1, 0 }, { 0, 1 }, { 0, -1 } };

    // ***[dfs]一次從一個點開始
    public static void dfs(int[][] matrix, boolean[][] visited, int[] startPos) {
        int iLen = matrix.length;
        int jLen = matrix[0].length;
        int iIndex = startPos[0];
        int jIndex = startPos[1];
        if (visited[iIndex][jIndex]) {
            return;
        }
        visited[iIndex][jIndex] = true;
        int val = matrix[iIndex][jIndex];
        int newVal = 0;

        // up, down, right, left
        for (int[] direction : directions) {
            int iTarget = iIndex + direction[0];
            int jTarget = jIndex + direction[1];
            if (0 <= iTarget && iTarget < iLen && 0 <= jTarget && jTarget < jLen && !visited[iTarget][jTarget]) {
                newVal = matrix[iTarget][jTarget];
                if (val <= newVal) {
                    dfs(matrix, visited, new int[] { iTarget, jTarget });
                }
            }
        }
    }

    // 1---------------------------------------------------------------------------------------------
    // ***[bfs] matrix
    public static List<int[]> pacificAtlantic(int[][] matrix) {
        // ***[array]用List<int[]>存二維陣列的點
        List<int[]> resultList = new ArrayList<int[]>();
        int iLen = matrix.length;
        if (iLen == 0) {
            return resultList;
        }
        int jLen = matrix[0].length;

        // ---------------
        boolean[][] visited = new boolean[iLen][jLen];
        // ***[queue] initialize queue
        Queue<int[]> queue = new LinkedList<int[]>();
        for (int i = 0; i < iLen; i++) {
            queue.add(new int[] { i, 0 });
        }
        for (int j = 0; j < jLen; j++) {
            queue.add(new int[] { 0, j });
        }
        bfs_matrix(matrix, queue, visited);
        // for (boolean[] a : visited) {
        // System.out.println(Arrays.toString(a));
        // }
        // --------------

        boolean[][] visitedBottom = new boolean[iLen][jLen];
        Queue<int[]> queueBottom = new LinkedList<int[]>();
        for (int i = 0; i < iLen; i++) {
            queueBottom.add(new int[] { i, jLen - 1 });
        }
        for (int j = 0; j < jLen; j++) {
            queueBottom.add(new int[] { iLen - 1, j });
        }

        bfs_matrix(matrix, queueBottom, visitedBottom);
        // for (boolean[] a : visitedBottom) {
        // System.out.println(Arrays.toString(a));
        // }

        for (int i = 0; i < iLen; i++) {
            for (int j = 0; j < jLen; j++) {
                if (visited[i][j] && visitedBottom[i][j]) {
                    resultList.add(new int[] { i, j });
                }
            }
        }
        return resultList;
    }

    // ***[bfs]一次從一個點開始、也可從多點開始
    public static void bfs_matrix(int[][] matrix, Queue<int[]> queue, boolean[][] visited) {
        int iLen = matrix.length;
        int jLen = matrix[0].length;
        while (!queue.isEmpty()) {
            int len = queue.size();
            for (int i = 0; i < len; i++) {
                int[] position = queue.poll();
                int iIndex = position[0];
                int jIndex = position[1];
                int currentVal = matrix[iIndex][jIndex];
                int newVal = 0;
                visited[iIndex][jIndex] = true;
                // up
                if (iIndex - 1 >= 0 && !visited[iIndex - 1][jIndex]) {
                    newVal = matrix[iIndex - 1][jIndex];
                    if (currentVal <= newVal) {
                        queue.add(new int[] { iIndex - 1, jIndex });
                    }
                }
                // left
                if (jIndex - 1 >= 0 && !visited[iIndex][jIndex - 1]) {
                    newVal = matrix[iIndex][jIndex - 1];
                    if (currentVal <= newVal) {
                        queue.add(new int[] { iIndex, jIndex - 1 });
                    }
                }
                // down
                if (iIndex + 1 < iLen && !visited[iIndex + 1][jIndex]) {
                    newVal = matrix[iIndex + 1][jIndex];
                    if (currentVal <= newVal) {
                        queue.add(new int[] { iIndex + 1, jIndex });
                    }
                }
                // right
                if (jIndex + 1 < jLen && !visited[iIndex][jIndex + 1]) {
                    newVal = matrix[iIndex][jIndex + 1];
                    if (currentVal <= newVal) {
                        queue.add(new int[] { iIndex, jIndex + 1 });
                    }
                }
            }
        }
    }

}
