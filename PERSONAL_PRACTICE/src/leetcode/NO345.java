package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NO345 {

    public static void main(String[] args) {
        System.out.println(reverseVowels("hello"));
    }

    // 1------------------------------------------------------------
    // beat 42%
    public static String reverseVowels(String s) {
        char[] charAry = s.toCharArray();
        int charAryLen = charAry.length;
        // @@@[list] linkedlist 超強 pollFirst, pollLast, 可是也會很慢
        List<Integer> vowelIndexList = new ArrayList<Integer>();
        // @@@[list] Arrays.asList('a','e','i','o','u', 'A','E','I','O','U');
        List<Character> vowels = Arrays.asList('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U');
        for (int i = 0; i < charAryLen; i++) {
            if (vowels.contains(charAry[i])) {
                vowelIndexList.add(i);
            }
        }
        int startIndex = 0;
        int endIndex = vowelIndexList.size() - 1;
        while (startIndex < endIndex) {
            swap(charAry, vowelIndexList.get(startIndex), vowelIndexList.get(endIndex));
            startIndex++;
            endIndex--;
        }
        return new String(charAry);
    }

    public static void swap(char[] charAry, int index1, int index2) {
        char c1 = charAry[index1];
        char c2 = charAry[index2];
        charAry[index1] = c2;
        charAry[index2] = c1;
    }

}
