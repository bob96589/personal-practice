package leetcode;

//TODO
public class NO263 {

    public static void main(String[] args) {

    }

    public static boolean isUgly2(int num) {

        if (num <= 0) {
            return false;
        }

        int[] uglyIntAry = { 2, 3, 5 };

        for (int uglyInt : uglyIntAry) {
            while (num % uglyInt == 0) {
                num /= uglyInt;
            }
        }

        if (num == 1) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean isUgly(int num) {

        if (num < 1) {
            return false;
        }
        if (num == 1) {
            return true;
        }

        if (num % 2 == 0) {
            return isUgly(num / 2);
        } else if (num % 3 == 0) {
            return isUgly(num / 3);
        } else if (num % 5 == 0) {
            return isUgly(num / 5);
        } else {
            return false;
        }

    }

}
