package wap2017;

import java.util.Scanner;

public class Test_2_2017_2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int cnt = in.nextInt();
        int divisor = in.nextInt();
        int[] nums = new int[cnt];
        for (int i = 0; i < cnt; i++) {
            nums[i] = in.nextInt();
        }
        long start = System.currentTimeMillis();
        int sum = 0;
        if (dfs(nums, -1, sum, divisor, 0)) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
        long end = System.currentTimeMillis();
        System.out.println((end - start) + " ms");
    }

    public static boolean dfs(int[] nums, int index, int sum, int divisor, int selectedCnt) {
        sum %= divisor;
        if (index >= 0 && sum == 0 && selectedCnt > 0) {
            return true;
        }
        int i = index + 1;
        if (i < nums.length) {
            // add
            if (dfs(nums, i, sum + nums[i], divisor, selectedCnt + 1)) {
                return true;
            }
            return dfs(nums, i, sum, divisor, selectedCnt);
        }
        return false;
    }

}
