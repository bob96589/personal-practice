package leetcode;

public class NO461 {

    public static void main(String[] args) {
        // @@@[bit manipulation] sum of 1 in binary string
        System.out.println(Integer.toBinaryString(0) + ": " + Integer.bitCount(0));
        System.out.println(Integer.toBinaryString(1) + ": " + Integer.bitCount(1));
        System.out.println(Integer.toBinaryString(2) + ": " + Integer.bitCount(2));
        System.out.println(Integer.toBinaryString(3) + ": " + Integer.bitCount(3));
        System.out.println(Integer.toBinaryString(4) + ": " + Integer.bitCount(4));
        System.out.println(Integer.toBinaryString(5) + ": " + Integer.bitCount(5));
        System.out.println(Integer.toBinaryString(6) + ": " + Integer.bitCount(6));
        System.out.println(Integer.toBinaryString(7) + ": " + Integer.bitCount(7));
    }

    public static int hammingDistance(int x, int y) { // beat 59%
        int temp = x ^ y;
        int cnt = 0;
        // @@@[bit manipulation] sum of 1 in binary string
        while (temp != 0) {
            cnt += (temp & 1);
            temp >>>= 1;
        }
        return cnt;
    }

    public static int hammingDistance2(int x, int y) { // beat 18%
        return Integer.bitCount(x ^ y);
    }

}
