package algorithm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Stack;

/**
 * 可檢查有沒有cycle postOrder DFS
 * 
 */
public class TOPOLOGICAL_SORT_DFS {

    public static class Node {
        public final String name;
        public final HashSet<Node> inNodes;
        public final HashSet<Node> outNodes;
        public boolean visited = false;

        public Node(String name) {
            this.name = name;
            inNodes = new HashSet<Node>();
            outNodes = new HashSet<Node>();
        }

        public Node addEdge(Node node) {
            this.outNodes.add(node);
            node.inNodes.add(this);
            return this;
        }

        @Override
        public boolean equals(Object obj) {
            Node node = (Node) obj;
            return name == node.name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    // recursive dfs
    private static void dfs(Node[] allNodes, Node node, Stack<Node> postOrderStack) {
        if (node.visited) {
            return;
        } else {
            node.visited = true;
        }

        HashSet<Node> outNodeSet = node.outNodes;
        for (Node outNode : outNodeSet) {
            if (!outNode.visited) {
                dfs(allNodes, outNode, postOrderStack);
            }
        }
        postOrderStack.push(node);
    }

    public static void main(String[] args) {
        Node node_7 = new Node("7");
        Node node_5 = new Node("5");
        Node node_3 = new Node("3");
        Node node_11 = new Node("11");
        Node node_8 = new Node("8");
        Node node_2 = new Node("2");
        Node node_9 = new Node("9");
        Node node_10 = new Node("10");
        node_7.addEdge(node_11).addEdge(node_8);
        node_5.addEdge(node_11);
        node_3.addEdge(node_8).addEdge(node_10);
        node_11.addEdge(node_2).addEdge(node_9).addEdge(node_10);
        node_8.addEdge(node_9).addEdge(node_10);

        // Node[] allNodes = { node_7, node_11, node_8};
        Node[] allNodes = { node_7, node_5, node_3, node_11, node_8, node_2, node_9, node_10 };
        // topoSortList <- Empty list that will contain the sorted elements
        List<Node> topoSortList = new ArrayList<Node>();

        Stack<Node> postOrderStack = new Stack<Node>();
        for (Node node : allNodes) {
            dfs(allNodes, node, postOrderStack);
        }
        while (!postOrderStack.isEmpty()) {
            System.out.println(postOrderStack.pop());
        }
    }
}