package algorithm;

public class BIT_MANIPULATION {

    // 位元運算子(Bitwise operator)
    // op1 & op2
    // op1 | op2
    // op1 ^ op2
    // ~op1
    // 取x的負值，~x+1 = -x
    // http://goodideascome.blogspot.tw/2011/08/2.html
    // ---------------------------------------------------
    // 位移運算子(Shift operator)
    // op1 << op2 將 op1 的位元左移 op2 個單位,右邊補上 0
    // op1 >> op2 將 op1 的位元右移 op2 個單位,左邊補上原來最左邊的位元值
    // op1 >>> op2 將 op1 的位元右移 op2 個單位,左邊補上 0

    public static void main(String[] args) {

        System.out.println(0x55555555);// hex string
        System.out.println(Integer.toHexString(1431655765));
        System.out.println("===========================================================");
        // 1431655765
        // 55555555

        // @@@[bit manipulation] sum of 1 in binary string
        System.out.println(Integer.toBinaryString(0) + ": " + Integer.bitCount(0));
        System.out.println(Integer.toBinaryString(1) + ": " + Integer.bitCount(1));
        System.out.println(Integer.toBinaryString(2) + ": " + Integer.bitCount(2));
        System.out.println(Integer.toBinaryString(3) + ": " + Integer.bitCount(3));
        System.out.println(Integer.toBinaryString(4) + ": " + Integer.bitCount(4));
        System.out.println(Integer.toBinaryString(5) + ": " + Integer.bitCount(5));
        System.out.println("===========================================================");
        // ===========================================================
        // 0: 0
        // 1: 1
        // 10: 1
        // 11: 2
        // 100: 1
        // 101: 2

        int a = 1;
        System.out.println("a <<= 1: " + (a <<= 1) + "; a: " + Integer.toBinaryString(a));
        System.out.println("a <<= 1: " + (a <<= 1) + "; a: " + Integer.toBinaryString(a));
        System.out.println("a <<= 1: " + (a <<= 1) + "; a: " + Integer.toBinaryString(a));
        System.out.println("a <<= 1: " + (a <<= 1) + "; a: " + Integer.toBinaryString(a));
        System.out.println("a <<= 1: " + (a <<= 1) + "; a: " + Integer.toBinaryString(a));
        System.out.println("===========================================================");
        // ===========================================================
        // a <<= 1: 2; a: 10
        // a <<= 1: 4; a: 100
        // a <<= 1: 8; a: 1000
        // a <<= 1: 16; a: 10000
        // a <<= 1: 32; a: 100000

        int b = -Integer.MAX_VALUE;
        // >> with sign
        System.out.println("b >>= 1: " + (b >>= 1) + "; b: " + Integer.toBinaryString(b));
        System.out.println("b >>= 1: " + (b >>= 1) + "; b: " + Integer.toBinaryString(b));
        System.out.println("b >>= 1: " + (b >>= 1) + "; b: " + Integer.toBinaryString(b));
        System.out.println("b >>= 1: " + (b >>= 1) + "; b: " + Integer.toBinaryString(b));
        System.out.println("b >>= 1: " + (b >>= 1) + "; b: " + Integer.toBinaryString(b));
        System.out.println("===========================================================");
        // ===========================================================
        // b >>= 1: -1073741824; b: 11000000000000000000000000000000
        // b >>= 1: -536870912; b: 11100000000000000000000000000000
        // b >>= 1: -268435456; b: 11110000000000000000000000000000
        // b >>= 1: -134217728; b: 11111000000000000000000000000000
        // b >>= 1: -67108864; b: 11111100000000000000000000000000

        // int c = 63;
        // >>> unsign
        int c = -Integer.MAX_VALUE;
        System.out.println("c >>>= 1: " + (c >>>= 1) + "; c: " + Integer.toBinaryString(c));
        System.out.println("c >>>= 1: " + (c >>>= 1) + "; c: " + Integer.toBinaryString(c));
        System.out.println("c >>>= 1: " + (c >>>= 1) + "; c: " + Integer.toBinaryString(c));
        System.out.println("c >>>= 1: " + (c >>>= 1) + "; c: " + Integer.toBinaryString(c));
        System.out.println("c >>>= 1: " + (c >>>= 1) + "; c: " + Integer.toBinaryString(c));
        System.out.println("===========================================================");
        // ===========================================================
        // c >>>= 1: 1073741824; c: 1000000000000000000000000000000
        // c >>>= 1: 536870912; c: 100000000000000000000000000000
        // c >>>= 1: 268435456; c: 10000000000000000000000000000
        // c >>>= 1: 134217728; c: 1000000000000000000000000000
        // c >>>= 1: 67108864; c: 100000000000000000000000000

    }

}
