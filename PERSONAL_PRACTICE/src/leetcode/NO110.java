package leetcode;

import leetcode.NO104.TreeNode;

//ok
public class NO110 {

    public static void main(String[] args) {
        TreeNode t1 = new TreeNode(1);
        TreeNode t2 = new TreeNode(2);
        TreeNode t3 = new TreeNode(3);

    }

    public static boolean isBalanced(TreeNode root) {
        if (root == null) {
            return true;
        }
        int leftHeight = maxDepth(root.left);
        int rightHeight = maxDepth(root.right);
        // ***[int]�����Math.abs
        int temp = Math.abs(leftHeight - rightHeight);
        if (temp == 0 || temp == 1) {
            return isBalanced(root.left) && isBalanced(root.right);
        } else {
            return false;
        }

    }

    private static int maxDepth(TreeNode root) {// beat 18%
        if (root == null) {
            return 0;
        }
        int leftHeight = maxDepth(root.left);
        int rightHeight = maxDepth(root.right);
        return Math.max(leftHeight, rightHeight) + 1;
    }

}
