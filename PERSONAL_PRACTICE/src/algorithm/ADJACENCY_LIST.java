package algorithm;

import java.util.ArrayList;
import java.util.HashMap;

public class ADJACENCY_LIST {
    public static void main(String[] args) {

    }

    public static void adjacencyList(String[] args) {
        ArrayList<ArrayList<Integer>> adjLists = new ArrayList<ArrayList<Integer>>();
        int n = 6;
        for (int i = 0; i < n; i++) {
            adjLists.add(new ArrayList<Integer>());
        }
        adjLists.get(0).add(1);
        adjLists.get(0).add(2);
        adjLists.get(1).add(2);
        adjLists.get(1).add(3);
        adjLists.get(2).add(4);
        adjLists.get(3).add(4);
        adjLists.get(3).add(5);
        adjLists.get(4).add(5);
        System.out.println("Print all adjacency lists with corresponding vertex:");
        for (int i = 0; i < n; i++) {
            System.out.println(i + ": " + adjLists.get(i));
        }
    }

    public static void adjacencyList2(String[] args) {
        HashMap<Integer, ArrayList<Integer>> adjLists = new HashMap<Integer, ArrayList<Integer>>();
        int n = 6;
        for (int i = 0; i < n; i++) {
            adjLists.put(i, new ArrayList<Integer>());
        }
        adjLists.get(0).add(1);
        adjLists.get(0).add(2);
        adjLists.get(1).add(2);
        adjLists.get(1).add(3);
        adjLists.get(2).add(4);
        adjLists.get(3).add(4);
        adjLists.get(3).add(5);
        adjLists.get(4).add(5);
        System.out.println("Print all adjacency lists with corresponding vertex:");
        for (int i = 0; i < n; i++) {
            System.out.println(i + ": " + adjLists.get(i));
        }

    }

}
