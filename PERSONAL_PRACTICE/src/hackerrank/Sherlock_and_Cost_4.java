package hackerrank;

import java.util.Scanner;

public class Sherlock_and_Cost_4 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            int k = in.nextInt();
            int[] ary = new int[k];
            for (int j = 0; j < k; j++) {
                ary[j] = in.nextInt();
            }
            print(ary);
        }
    }

    public static void print(int[] nums) {
        // int[] nums = new int[] { 10, 1, 10, 1, 10 };
        // int[] nums = new int[] { 100, 2, 100, 2, 100 };

        int len = nums.length;
        int[] score1 = new int[len];
        int[] score2 = new int[len];

        for (int i = 1; i < len; i++) {
            score1[i] = Math.max(score1[i - 1] + getScore(1, 1), score2[i - 1] + getScore(1, nums[i - 1]));
            score2[i] = Math.max(score1[i - 1] + getScore(nums[i], 1), score2[i - 1] + getScore(nums[i], nums[i - 1]));
        }

        System.out.println(Math.max(score1[len - 1], score2[len - 1]));
    }

    public static int getScore(int a, int b) {
        return a > b ? a - b : b - a;
    }

}
