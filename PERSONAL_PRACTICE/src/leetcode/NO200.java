package leetcode;

import java.util.LinkedList;
import java.util.Queue;

public class NO200 {

    public static void main(String[] args) {
        char[][] grid = new char[][] { { '1', '1', '0', '0', '0' }, { '1', '1', '0', '0', '0' }, { '0', '0', '1', '0', '0' }, { '0', '0', '0', '1', '1' }, };
        // System.out.println(numIslands(grid));
        System.out.println(numIslands2(grid));
    }

    // 2-------------------------------------------------------------------------------
    // dsf: 每個都靠太近了，用DFS快
    // beat 94%
    public static int numIslands2(char[][] grid) {
        int iLen = grid.length;
        if (iLen == 0) {
            return 0;
        }
        int jLen = grid[0].length;
        int count = 0;
        for (int i = 0; i < iLen; i++) {
            for (int j = 0; j < jLen; j++) {
                if (grid[i][j] == '1') {
                    count++;
                    dfs_matrix(grid, i, j);
                }
            }
        }
        return count;
    }

    public static void dfs_matrix(char[][] grid, int iIndex, int jIndex) {
        int iLen = grid.length;
        int jLen = grid[0].length;
        grid[iIndex][jIndex] = '0';
        // up
        if (iIndex - 1 >= 0 && grid[iIndex - 1][jIndex] == '1') {
            dfs_matrix(grid, iIndex - 1, jIndex);
        }
        // left
        if (jIndex - 1 >= 0 && grid[iIndex][jIndex - 1] == '1') {
            dfs_matrix(grid, iIndex, jIndex - 1);
        }
        // down
        if (iIndex + 1 < iLen && grid[iIndex + 1][jIndex] == '1') {
            dfs_matrix(grid, iIndex + 1, jIndex);
        }
        // right
        if (jIndex + 1 < jLen && grid[iIndex][jIndex + 1] == '1') {
            dfs_matrix(grid, iIndex, jIndex + 1);
        }
    }

    // 1-------------------------------------------------------------------------------
    // bfs
    // beat 16%
    public static int numIslands(char[][] grid) {
        int iLen = grid.length;
        if (iLen == 0) {
            return 0;
        }
        int jLen = grid[0].length;
        Queue<int[]> queue;
        int count = 0;
        for (int i = 0; i < iLen; i++) {
            for (int j = 0; j < jLen; j++) {
                if (grid[i][j] == '1') {
                    count++;
                    grid[i][j] = '0';
                    queue = new LinkedList<int[]>();
                    queue.add(new int[] { i, j });
                    bfs_matrix(grid, queue);
                }
            }
        }
        return count;
    }

    public static void bfs_matrix(char[][] grid, Queue<int[]> queue) {
        int iLen = grid.length;
        int jLen = grid[0].length;
        while (!queue.isEmpty()) {
            int len = queue.size();
            for (int i = 0; i < len; i++) {
                int[] position = queue.poll();
                int iIndex = position[0];
                int jIndex = position[1];
                // up
                if (iIndex - 1 >= 0 && grid[iIndex - 1][jIndex] == '1') {
                    queue.add(new int[] { iIndex - 1, jIndex });
                    grid[iIndex - 1][jIndex] = '0';
                }
                // left
                if (jIndex - 1 >= 0 && grid[iIndex][jIndex - 1] == '1') {
                    queue.add(new int[] { iIndex, jIndex - 1 });
                    grid[iIndex][jIndex - 1] = '0';
                }
                // down
                if (iIndex + 1 < iLen && grid[iIndex + 1][jIndex] == '1') {
                    queue.add(new int[] { iIndex + 1, jIndex });
                    grid[iIndex + 1][jIndex] = '0';
                }
                // right
                if (jIndex + 1 < jLen && grid[iIndex][jIndex + 1] == '1') {
                    queue.add(new int[] { iIndex, jIndex + 1 });
                    grid[iIndex][jIndex + 1] = '0';
                }
            }
        }
    }

}
