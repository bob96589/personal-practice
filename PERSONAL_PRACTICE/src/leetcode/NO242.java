package leetcode;

import java.util.Arrays;

//TODO
public class NO242 {

    public static void main(String[] args) {

    }

    // 2---------------------------------------------------------------------------
    public static boolean isAnagram2(String s, String t) {
        // @@@[array]先增加後刪除
        // @@@[array] 字母相關，用array處理
        int[] alphabet = new int[26];
        for (int i = 0; i < s.length(); i++) {
            alphabet[s.charAt(i) - 'a']++;
        }
        for (int i = 0; i < t.length(); i++) {
            alphabet[t.charAt(i) - 'a']--;
        }
        for (int i : alphabet) {
            if (i != 0) {
                return false;
            }
        }
        return true;
    }

    // 1---------------------------------------------------------------------------
    public static boolean isAnagram(String s, String t) {

        // @@@[string] sort char in string, char[], Arrays.sort(sAry);
        char[] sAry = s.toCharArray();
        char[] tAry = t.toCharArray();
        int sLength = sAry.length;
        int tLength = tAry.length;
        if (sLength != tLength) {
            return false;
        }

        if (sLength == 0) {
            return true;
        }
        Arrays.sort(sAry);
        Arrays.sort(tAry);
        for (int i = 0; i < sLength; i++) {
            if (sAry[i] != tAry[i]) {
                return false;
            }
        }
        return true;

    }

}
