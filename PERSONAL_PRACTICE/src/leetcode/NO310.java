package leetcode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

//***[topological sort]important, simialr to topological sort , 從left摘除
public class NO310 {

    public static void main(String[] args) {
        // System.out.println(findMinHeightTrees2(1, new int[][] {}));
        // System.out.println(findMinHeightTrees2(4, new int[][] { { 1, 0 }, {
        // 1, 2 }, { 1, 3 } }));
        System.out.println(findMinHeightTrees2(6, new int[][] { { 3, 0 }, { 3, 1 }, { 3, 2 }, { 3, 4 }, { 5, 4 } }));
    }

    // 2---------------------------------------------------------------------------------
    public static List<Integer> findMinHeightTrees2(int n, int[][] edges) {
        List<Integer> list = new ArrayList<Integer>();
        if (n == 1) {
            list.add(0);
            return list;
        }
        // adjList
        Map<Integer, List<Integer>> adjListMap = new HashMap<Integer, List<Integer>>();
        for (int[] edge : edges) {
            if (!adjListMap.containsKey(edge[0])) {
                adjListMap.put(edge[0], new ArrayList<Integer>());
            }
            adjListMap.get(edge[0]).add(edge[1]);
            if (!adjListMap.containsKey(edge[1])) {
                adjListMap.put(edge[1], new ArrayList<Integer>());
            }
            adjListMap.get(edge[1]).add(edge[0]);
        }
        // delete leaves
        List<Integer> leaves = new LinkedList<Integer>();
        List<Integer> nextLeaves = new LinkedList<Integer>();
        Set<Integer> keys = adjListMap.keySet();
        for (Integer key : keys) {
            if (adjListMap.get(key).size() <= 1) {
                leaves.add(key);
            }
        }
        while (true) {
            int leavesCnt = leaves.size();
            if (leavesCnt == 1) {
                list.add(leaves.get(0));
                return list;
            } else if (leavesCnt == 2) {
                Integer first = leaves.get(0);
                Integer second = leaves.get(1);
                if (adjListMap.get(first).get(0).equals(second)) {
                    list.add(leaves.get(0));
                    list.add(leaves.get(1));
                    return list;
                }
            }
            for (Integer leaf : leaves) {
                Integer nextLeaf = adjListMap.get(leaf).get(0);
                adjListMap.get(nextLeaf).remove(leaf);
                if (adjListMap.get(nextLeaf).size() == 1) {
                    nextLeaves.add(nextLeaf);
                }
            }
            leaves = nextLeaves;
            nextLeaves = new LinkedList<Integer>();
        }
    }

    // 1---------------------------------------------------------------------------------
    public static List<Integer> findMinHeightTrees(int n, int[][] edges) {
        List<Integer> list = new ArrayList<Integer>();
        if (n == 1) {
            list.add(0);
            return list;
        }
        // adjList
        Map<Integer, List<Integer>> adjListMap = new HashMap<Integer, List<Integer>>();
        for (int[] edge : edges) {
            if (!adjListMap.containsKey(edge[0])) {
                adjListMap.put(edge[0], new ArrayList<Integer>());
            }
            adjListMap.get(edge[0]).add(edge[1]);
            if (!adjListMap.containsKey(edge[1])) {
                adjListMap.put(edge[1], new ArrayList<Integer>());
            }
            adjListMap.get(edge[1]).add(edge[0]);
        }
        // delete leaves
        Queue<Integer> leaves = new LinkedList<Integer>();
        Set<Integer> keys = adjListMap.keySet();
        for (Integer key : keys) {
            if (adjListMap.get(key).size() <= 1) {
                leaves.add(key);
            }
        }

        // ***[Iterator]不能新增，僅能刪除
        while (true) {
            int cnt = leaves.size();
            if (cnt == 1) {
                list.add(leaves.peek());
                return list;
            } else if (cnt == 2) {
                // ***[colleciton] collection to array
                Integer[] intAry = new Integer[2];
                leaves.toArray(intAry);
                // ***[int]Integer相等，用equals
                if (adjListMap.get(intAry[0]).get(0).equals(intAry[1])) {
                    list.add(intAry[0]);
                    list.add(intAry[1]);
                    return list;
                }
            }
            for (int i = 0; i < cnt; i++) {
                Integer leaf = leaves.poll();
                Integer newLeaf = adjListMap.get(leaf).get(0);
                adjListMap.get(newLeaf).remove(leaf);
                if (adjListMap.get(newLeaf).size() == 1) {
                    leaves.add(newLeaf);
                }
            }
        }
    }
}
