package leetcode;

import java.util.ArrayList;
import java.util.List;

public class NO119 {

    public static void main(String[] args) {
        System.out.println(getRow(0));
        System.out.println(getRow(1));
        System.out.println(getRow(2));
        System.out.println(getRow(3));
        System.out.println(getRow(4));
    }

    // 2-----------------------------------------------------------------------------
    // [1]
    // [1, 1]
    // [1, 2, 1]
    // [1, 3, 3, 1]
    // [1, 4, 6, 4, 1]
    public List<Integer> getRow2(int rowIndex) {
        List<Integer> list = new ArrayList<Integer>();
        if (rowIndex < 0)
            return list;

        for (int i = 0; i < rowIndex + 1; i++) {
            // @@@[list] insert item in index, and shift other
            list.add(0, 1);
            for (int j = 1; j < list.size() - 1; j++) {
                // @@@[list] replace item in index
                list.set(j, list.get(j) + list.get(j + 1));
            }
        }
        return list;
    }

    // 1-----------------------------------------------------------------------------
    // too slow, beat 0%
    public static List<Integer> getRow(int rowIndex) {
        memo = new Integer[1000][1000];
        List<Integer> result = new ArrayList<>(rowIndex + 1);

        int indexCnt = rowIndex / 2;
        int remainder = rowIndex % 2;
        for (int i = 0; i <= indexCnt; i++) {
            result.add(cFunction(rowIndex, i));
        }
        int startIndex;
        if (remainder == 0) {
            startIndex = indexCnt - 1;
        } else {
            startIndex = indexCnt;
        }
        for (int j = startIndex; j >= 0; j--) {
            result.add(new Integer(result.get(j)));
        }
        return result;
    }

    static Integer[][] memo;

    public static int cFunction(int a, int b) {
        if (a == b) {
            return 1;
        }
        if (b == 1) {
            return a;
        } else if (b == 0) {
            return 1;
        }
        if (memo[a][b] != null) {
            return memo[a][b];
        }

        int result = cFunction(a - 1, b) + cFunction(a - 1, b - 1);
        memo[a][b] = result;
        return result;
    }

}
