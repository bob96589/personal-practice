package tao;

public class Tao003 {
    public static void main(String[] args) {
        int[] input = new int[] { 2018, 2032, 2054, 2200, 5566, 77, 7, 87, 9, 22 };
        printChineseAnimalYear(input);
    }

    public static void printChineseAnimalYear(int[] input) {
        String[] animal = new String[] { "��", "��", "��", "��", "�s", "�D", "��", "��", "�U", "��", "��", "��" };
        for (int year : input) {
            System.out.println(animal[(year - 4) % 12]);
        }
    }
}
