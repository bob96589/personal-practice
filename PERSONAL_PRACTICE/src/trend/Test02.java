package trend;

import java.util.HashSet;
import java.util.Set;

public class Test02 {

    public static void main(String[] args) {
        // int[] A = new int[]{60,80,40};
        // int[] B = new int[]{2,3,5};
        // int M = 5;
        // int X = 2;
        // int Y = 200;

        int[] A = new int[] {};
        int[] B = new int[] {};
        int M = 0;
        int X = 0;
        int Y = 0;
        System.out.println(solution(A, B, M, X, Y));
    }

    public static int solution(int[] A, int[] B, int M, int X, int Y) {

        int stopCnt = 0;

        Set<Integer> set = new HashSet<Integer>();
        int peopleCnt = 0;
        int weightCnt = 0;
        int len = A.length;
        for (int i = 0; i < len; i++) {
            if (peopleCnt + 1 > X || weightCnt + A[i] > Y) {
                stopCnt += set.size() + 1;

                peopleCnt = 0;
                weightCnt = 0;
                set.clear();

            }
            peopleCnt++;
            weightCnt += A[i];
            set.add(B[i]);

        }
        if (set.size() != 0) {
            stopCnt += set.size() + 1;
        }
        return stopCnt;
    }

}
