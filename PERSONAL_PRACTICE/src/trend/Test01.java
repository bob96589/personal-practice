package trend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Test01 {

    public static void main(String[] args) {
        String S = "John Doe; Peter Benjamin Parker; Mary Jane Watson-Parker; John Elvis Doe; John Evan Doe; Jane Doe; Peter Brian Parker";
        String C = "Example";
        System.out.println(solution(S, C));
    }

    public static String solution(String S, String C) {

        List<String> list = new ArrayList<String>();
        Map<String, Integer> map = new HashMap<String, Integer>();
        C = C.toLowerCase();

        String[] nameAry = S.split("; ");
        for (String str : nameAry) {
            String[] temp = str.split(" ");
            int len = temp.length;

            String[] part = new String[len];
            for (int i = 0; i < len; i++) {
                if (i == 0) {
                    part[0] = temp[len - 1];
                } else {
                    part[i] = temp[i - 1];
                }
            }

            if (len == 3) {
                part[2] = part[2].substring(0, 1);
            }

            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < len; i++) {
                if (i != 0) {
                    sb.append("_");
                }
                sb.append(part[i]);
            }

            String result = sb.toString();

            // remove last name -
            result = result.replace("-", "");
            // lower case
            result = result.toLowerCase();

            if (map.containsKey(result)) {
                int num = map.get(result).intValue() + 1;
                map.put(result, num);
                result = result + num;
            } else {
                map.put(result, 1);
            }
            list.add(result);

        }

        StringBuffer strBuff = new StringBuffer();

        for (int i = 0; i < list.size(); i++) {
            if (i != 0) {
                strBuff.append("; ");
            }
            strBuff.append(list.get(i));
            strBuff.append("@");
            strBuff.append(C);
            strBuff.append(".com");
        }

        String finalResult = strBuff.toString();

        return finalResult;
    }

}
