package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NO349 {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

    // 3-------------------------------------------------------------------
    // beat 30%
    public static int[] intersection3(int[] nums1, int[] nums2) {
        Arrays.sort(nums2);
        int len2 = nums2.length;
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < nums1.length; i++) {
            int val = nums1[i];
            if (list.contains(val)) {
                continue;
            }
            int startIndex = 0;
            int endIndex = len2 - 1;
            while (startIndex <= endIndex) {
                int mid = (startIndex + endIndex) / 2;
                if (val == nums2[mid]) {
                    list.add(val);
                    break;
                } else if (val < nums2[mid]) {
                    endIndex = mid - 1;
                } else if (val > nums2[mid]) {
                    startIndex = mid + 1;
                }
            }
        }

        int[] result = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            result[i] = list.get(i);
        }
        return result;

    }

    // 2-------------------------------------------------------------------
    // beat 15%
    public static int[] intersection2(int[] nums1, int[] nums2) {
        Arrays.sort(nums2);
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < nums1.length; i++) {
            if (list.contains(nums1[i])) {
                continue;
            }
            if (Arrays.binarySearch(nums2, nums1[i]) >= 0) {
                list.add(nums1[i]);
            }
        }

        int[] result = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            result[i] = list.get(i);
        }
        return result;

    }

    // 1-------------------------------------------------------------------
    // beat 15%
    public static int[] intersection(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        Set<Integer> set = new HashSet<Integer>();
        int startJ = 0;
        for (int i = 0; i < nums1.length; i++) {
            for (int j = startJ; j < nums2.length; j++) {
                if (nums1[i] == nums2[j]) {
                    set.add(nums1[i]);
                    startJ = j;
                    break;
                } else if (nums1[i] < nums2[j]) {
                    startJ = j;
                    break;
                }
            }
        }
        // @@@[set] set<integer> to int[]: set >> list >> forloop int[]
        int[] resultAry = new int[set.size()];
        List<Integer> list = new ArrayList<>(set);
        for (int i = 0; i < resultAry.length; i++) {
            resultAry[i] = list.get(i);
        }
        return resultAry;
    }

}
