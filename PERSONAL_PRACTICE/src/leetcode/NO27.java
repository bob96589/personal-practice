package leetcode;

public class NO27 {

    public static void main(String[] args) {

    }

    public static int removeElement(int[] nums, int val) {
        int len = nums.length;
        int cnt = 0;
        // ***[array] remove element, generate for loop with shortcut
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[cnt++] = nums[i];
            }
        }
        return cnt;
    }

}
