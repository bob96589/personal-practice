package leetcode;

//ok
public class NO171 {

    public static void main(String[] args) {

        System.out.println('A' + 0);
        // A >> 65
        System.out.println('A' - 64);
    }

    public static int titleToNumber(String s) {
        char[] charAry = s.toCharArray();
        int pow = 1;
        int total = 0;
        char c;
        // ***[char] calculate
        for (int i = charAry.length - 1; i >= 0; i--) {
            c = charAry[i];
            total += (pow * (c - 64));
            pow *= 26;
        }
        return total;
    }

}
