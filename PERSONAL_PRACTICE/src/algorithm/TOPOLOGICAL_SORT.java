package algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * 可檢查有沒有cycle 拔掉連結 要雙邊太難了
 */
public class TOPOLOGICAL_SORT {

    public static class Node {
        public final String name;
        public final HashSet<Node> inNodes;
        public final HashSet<Node> outNodes;

        public Node(String name) {
            this.name = name;
            inNodes = new HashSet<Node>();
            outNodes = new HashSet<Node>();
        }

        public Node addEdge(Node node) {
            this.outNodes.add(node);
            node.inNodes.add(this);
            return this;
        }

        @Override
        public boolean equals(Object obj) {
            Node node = (Node) obj;
            return name == node.name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public static void main(String[] args) {
        Node node_7 = new Node("7");
        Node node_5 = new Node("5");
        Node node_3 = new Node("3");
        Node node_11 = new Node("11");
        Node node_8 = new Node("8");
        Node node_2 = new Node("2");
        Node node_9 = new Node("9");
        Node node_10 = new Node("10");
        node_7.addEdge(node_11).addEdge(node_8);
        node_5.addEdge(node_11);
        node_3.addEdge(node_8).addEdge(node_10);
        node_11.addEdge(node_2).addEdge(node_9).addEdge(node_10);
        node_8.addEdge(node_9).addEdge(node_10);

        // Node[] allNodes = { node_7, node_11, node_8};
        Node[] allNodes = { node_7, node_5, node_3, node_11, node_8, node_2, node_9, node_10 };
        // topoSortList <- Empty list that will contain the sorted elements
        List<Node> topoSortList = new ArrayList<Node>();

        // nodesWithoutInEdge <- Set of all nodes with no incoming edges
        HashSet<Node> nodesWithoutInNode = new HashSet<Node>();
        for (Node node : allNodes) {
            if (node.inNodes.size() == 0) {
                nodesWithoutInNode.add(node);
            }
        }

        // while nodesWithoutInEdge is non-empty do
        while (!nodesWithoutInNode.isEmpty()) {
            // remove a node n from S
            Node n = nodesWithoutInNode.iterator().next();
            nodesWithoutInNode.remove(n);

            // insert n into topoSortList
            topoSortList.add(n);

            // for each node m with an edge e from n to m do
            for (Iterator<Node> it = n.outNodes.iterator(); it.hasNext();) {
                // remove edge e from the graph
                Node m = it.next();
                it.remove();// Remove outNode from n
                m.inNodes.remove(n);// Remove inNode from m

                // if m has no other incoming edges then insert m into S
                if (m.inNodes.isEmpty()) {
                    nodesWithoutInNode.add(m);
                }
            }
        }
        // Check to see if all edges are removed
        boolean cycle = false;
        for (Node n : allNodes) {
            if (!n.inNodes.isEmpty()) {
                cycle = true;
                break;
            }
        }
        if (cycle) {
            System.out.println("Cycle present, topological sort not possible");
        } else {
            System.out.println("Topological Sort: " + Arrays.toString(topoSortList.toArray()));
        }
    }
}