package leetcode;

public class NO541 {

    // Given a string and an integer k, you need to reverse the first k
    // characters for every 2k characters counting from the start of the string.
    public static void main(String[] args) {
        // System.out.println(reverseStr("abcdefg", 2));
        // System.out.println(reverseStr2("abcdefg", 2));
        System.out.println(reverseStr2("a", 2));
    }

    // 2------------------------------------------------------------------------------
    public static String reverseStr2(String s, int k) {
        char[] charAry = s.toCharArray();
        int len = charAry.length;
        for (int i = 0; i < len; i += 2 * k) {
            int startIndex = i;
            int endIndex = i + k - 1 > len - 1 ? len - 1 : i + k - 1;
            // @@@[array] end situation
            while (startIndex < endIndex) {
                swap(charAry, startIndex++, endIndex--);
            }
        }
        // @@@[string] char[] to string
        return new String(charAry);
    }

    public static void swap(char[] charAry, int index1, int index2) {
        char c1 = charAry[index1];
        char c2 = charAry[index2];
        charAry[index1] = c2;
        charAry[index2] = c1;
    }

    // 1------------------------------------------------------------------------------
    public static String reverseStr(String s, int k) {
        int len = s.length();
        // @@@[for] ��m3�i��i += 2 * k
        for (int index = 0; index < len; index += 2 * k) {
            int startIndex = index;
            int endIndex = (index + k) > len ? len : (index + k);
            String partK = s.substring(startIndex, endIndex);
            StringBuffer sb = new StringBuffer(partK);
            partK = sb.reverse().toString();
            if (endIndex - startIndex < k) {
                s = s.substring(0, startIndex) + partK;
            } else {
                s = s.substring(0, startIndex) + partK + s.substring(endIndex);
            }
        }
        return s;
    }

}
