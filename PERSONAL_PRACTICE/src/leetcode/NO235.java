package leetcode;

//ok
public class NO235 {

    public static void main(String[] args) {

    }

    // ***[lowestCommonAncestor]
    // ***[tree] binary search tree要記得用此樹的特點
    public static TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) {
            return root;
        }
        int rootVal = root.val;
        int bigVal = p.val >= q.val ? p.val : q.val;
        int smallVal = p.val > q.val ? q.val : p.val;
        if (bigVal < rootVal) {
            return lowestCommonAncestor(root.left, p, q);
        } else if (rootVal < smallVal) {
            return lowestCommonAncestor(root.right, p, q);
        } else {
            return root;
        }
    }

    public static TreeNode lowestCommonAncestor1(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null) {
            return root;
        }
        TreeNode lowestAncestor = root;
        boolean flag = containsElement(lowestAncestor, p) && containsElement(lowestAncestor, q);
        while (flag) {
            flag = false;
            if (containsElement(lowestAncestor.left, p) && containsElement(lowestAncestor.left, q)) {
                flag = true;
                lowestAncestor = lowestAncestor.left;
            }
            if (containsElement(lowestAncestor.right, p) && containsElement(lowestAncestor.right, q)) {
                flag = true;
                lowestAncestor = lowestAncestor.right;
            }
        }
        return lowestAncestor;

    }

    // ***[tree] containsElement, recursion
    public static boolean containsElement(TreeNode root, TreeNode node) {
        if (root == null) {
            return false;
        }
        if (root.equals(node)) {
            return true;
        }
        if (containsElement(root.left, node) || containsElement(root.right, node)) {
            return true;
        }
        return false;
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

}
