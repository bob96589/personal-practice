package algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * �ӳ·СA��Edge Class
 *
 */
public class TOPOLOGICAL_SORT_EDGE_X {

    public static class Node {
        public final String name;
        public final HashSet<Edge> inEdges;
        public final HashSet<Edge> outEdges;

        public Node(String name) {
            this.name = name;
            inEdges = new HashSet<Edge>();
            outEdges = new HashSet<Edge>();
        }

        public Node addEdge(Node node) {
            Edge e = new Edge(this, node);
            outEdges.add(e);
            node.inEdges.add(e);
            return this;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    public static class Edge {
        public final Node from;
        public final Node to;

        public Edge(Node from, Node to) {
            this.from = from;
            this.to = to;
        }

        @Override
        public boolean equals(Object obj) {
            Edge e = (Edge) obj;
            return e.from == from && e.to == to;
        }
    }

    public static void main(String[] args) {
        Node node_7 = new Node("7");
        Node node_5 = new Node("5");
        Node node_3 = new Node("3");
        Node node_11 = new Node("11");
        Node node_8 = new Node("8");
        Node node_2 = new Node("2");
        Node node_9 = new Node("9");
        Node node_10 = new Node("10");
        node_7.addEdge(node_11).addEdge(node_8);
        node_5.addEdge(node_11);
        node_3.addEdge(node_8).addEdge(node_10);
        node_11.addEdge(node_2).addEdge(node_9).addEdge(node_10);
        node_8.addEdge(node_9).addEdge(node_10);

        Node[] allNodes = { node_7, node_5, node_3, node_11, node_8, node_2, node_9, node_10 };
        // topoSortList <- Empty list that will contain the sorted elements
        List<Node> topoSortList = new ArrayList<Node>();

        // nodesWithoutInEdge <- Set of all nodes with no incoming edges
        HashSet<Node> nodesWithoutInEdge = new HashSet<Node>();
        for (Node node : allNodes) {
            if (node.inEdges.size() == 0) {
                nodesWithoutInEdge.add(node);
            }
        }

        // while nodesWithoutInEdge is non-empty do
        while (!nodesWithoutInEdge.isEmpty()) {
            // remove a node n from S
            Node n = nodesWithoutInEdge.iterator().next();
            nodesWithoutInEdge.remove(n);

            // insert n into topoSortList
            topoSortList.add(n);

            // for each node m with an edge e from n to m do
            for (Iterator<Edge> it = n.outEdges.iterator(); it.hasNext();) {
                // remove edge e from the graph
                Edge e = it.next();
                Node m = e.to;
                it.remove();// Remove edge from n
                m.inEdges.remove(e);// Remove edge from m

                // if m has no other incoming edges then insert m into S
                if (m.inEdges.isEmpty()) {
                    nodesWithoutInEdge.add(m);
                }
            }
        }
        // Check to see if all edges are removed
        boolean cycle = false;
        for (Node n : allNodes) {
            if (!n.inEdges.isEmpty()) {
                cycle = true;
                break;
            }
        }
        if (cycle) {
            System.out.println("Cycle present, topological sort not possible");
        } else {
            System.out.println("Topological Sort: " + Arrays.toString(topoSortList.toArray()));
        }
    }
}