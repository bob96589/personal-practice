package leetcode;

public class NO5 {

    public static void main(String[] args) {
        System.out.println(longestPalindrome("cbbd"));
    }

    // @@@[variable]使用類別變數，要初始化
    static int maxCnt = 0;
    static String resultStr = "";

    public static String longestPalindrome(String s) {
        // @@@[variable]使用類別變數，要初始化
        maxCnt = 0;
        resultStr = "";
        char[] charAry = s.toCharArray();
        for (int i = 0; i < charAry.length; i++) {
            findPalindrome(charAry, i, i);
            findPalindrome(charAry, i, i + 1);

        }
        return resultStr;
    }

    // @@@[string]遇回文，從回文中心向外擴散
    private static void findPalindrome(char[] charAry, int leftIndex, int rightIndex) {
        while (0 <= leftIndex && rightIndex < charAry.length) {
            if (charAry[leftIndex] == charAry[rightIndex]) {
                leftIndex--;
                rightIndex++;
            } else {
                break;
            }
        }
        if (maxCnt < rightIndex - leftIndex - 1) {
            maxCnt = rightIndex - leftIndex - 1;
            resultStr = new String(charAry).substring(leftIndex + 1, rightIndex);
        }
    }

}
