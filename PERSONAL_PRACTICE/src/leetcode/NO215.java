package leetcode;

import java.util.Arrays;

public class NO215 {

    public static void main(String[] args) {

    }

    public static int findKthLargest(int[] nums, int k) {
        int len = nums.length;
        Arrays.sort(nums);
        return nums[len - k];
    }

}
