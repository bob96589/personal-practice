package tao;

public class Tao012 {
    public static void main(String[] args) {
        double num = 1000000;

        double everyMonth = ((5d / 12d) + 100d) / 100d;
        int monthCnt = 10 * 12;
        for (int i = 0; i < monthCnt; i++) {
            num *= everyMonth;
        }
        System.out.println(num);
        System.out.printf("%.3f", num);

    }
}
