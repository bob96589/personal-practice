package leetcode;

//ok
public class NO231 {

    public static void main(String[] args) {

    }

    public boolean isPowerOfTwo(int n) {
        if (n == 0) {
            return false;
        } else if (n == 1) {
            return true;
        }
        while (true) {
            if (n % 2 == 0) {
                n /= 2;
                if (n == 1) {
                    return true;
                }
            } else {
                return false;
            }

        }

    }

}
