package leetcode;

import java.util.ArrayList;
import java.util.List;

public class NO113 {

    public static void main(String[] args) {

    }

    public static List<List<Integer>> pathSum(TreeNode root, int sum) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        if (root == null) {
            return result;
        }
        List<Integer> temp = new ArrayList<Integer>();
        temp.add(root.val);
        int tempSum = root.val;
        dfs(root, sum, result, temp, tempSum);
        return result;
    }

    private static void dfs(TreeNode root, int sum, List<List<Integer>> result, List<Integer> temp, int tempSum) {
        if (root.left == null && root.right == null) {
            if (sum == tempSum) {
                result.add(new ArrayList(temp));
            }
        }
        if (root.left != null) {
            temp.add(root.left.val);
            dfs(root.left, sum, result, temp, tempSum + root.left.val);
            temp.remove(temp.size() - 1);
        }
        if (root.right != null) {
            temp.add(root.right.val);
            dfs(root.right, sum, result, temp, tempSum + root.right.val);
            temp.remove(temp.size() - 1);
        }
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

}
