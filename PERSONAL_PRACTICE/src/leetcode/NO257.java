package leetcode;

import java.util.ArrayList;
import java.util.List;

public class NO257 {
    public static void main(String[] args) {
        TreeNode node_1 = new TreeNode(1);
        TreeNode node_2 = new TreeNode(2);
        TreeNode node_3 = new TreeNode(3);
        TreeNode node_4 = new TreeNode(4);
        node_1.left = node_2;
        node_1.left.left = node_3;
        node_1.right = node_4;
        System.out.println(binaryTreePaths(node_1));
    }

    // ***[dfs] binary tree dfs
    private static void dfs(TreeNode root, List<String> memo, String str) {
        if (root == null) {
            return;
        }
        // -------------------------------------
        // preOrder: preOrder的東西，可移至inOrder的dfs()中做處理
        if (!str.isEmpty()) {
            str += "->";
        }
        str += root.val;
        if (root.left == null && root.right == null) { // leaf
            memo.add(str);
        }
        // -------------------------------------
        // inOrder:
        if (root.left != null) {
            dfs(root.left, memo, str);
        }
        if (root.right != null) {
            dfs(root.right, memo, str);
        }
        // -------------------------------------
        // postOrder:
        if (str.lastIndexOf("->") == -1) {
            str = "";
        } else {
            str = str.substring(0, str.lastIndexOf("->"));
        }
    }

    // ***[dfs] binary tree dfs, better
    private void dfs2(TreeNode root, List<String> memo, String str) {
        if (root.left == null && root.right == null) {
            memo.add(str + root.val);
        }
        if (root.left != null) {
            dfs2(root.left, memo, str + root.val + "->");
        }
        if (root.right != null) {
            dfs2(root.right, memo, str + root.val + "->");
        }
    }

    public static List<String> binaryTreePaths(TreeNode root) {
        List<String> memo = new ArrayList<String>();
        dfs(root, memo, "");
        return memo;
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }

        @Override
        public String toString() {
            return Integer.toString(val);
        }
    }
}
