package leetcode;

//ok
public class NO357 {

    public static void main(String[] args) {
        System.out.println(countNumbersWithUniqueDigits(0));
        System.out.println(countNumbersWithUniqueDigits(1));
        System.out.println(countNumbersWithUniqueDigits(2));
        System.out.println(countNumbersWithUniqueDigits(3));
        System.out.println(countNumbersWithUniqueDigits(4));
        System.out.println("---------------");
        System.out.println(countNumbersWithUniqueDigits3(0));
        System.out.println(countNumbersWithUniqueDigits3(1));
        System.out.println(countNumbersWithUniqueDigits3(2));
        System.out.println(countNumbersWithUniqueDigits3(3));
        System.out.println(countNumbersWithUniqueDigits3(4));
        System.out.println("---------------");
        System.out.println(countNumbersWithUniqueDigits4(0));
        System.out.println(countNumbersWithUniqueDigits4(1));
        System.out.println(countNumbersWithUniqueDigits4(2));
        System.out.println(countNumbersWithUniqueDigits4(3));
        System.out.println(countNumbersWithUniqueDigits4(4));
    }

    // 10^1: 0-9: 10
    // 10^2: 10-99: 9*9
    // 10^3: 100-999: 9*9*8
    // 10^4: 1000-9999: 9*9*8*7

    // 4------------------------------------------------------------------------
    public static int countNumbersWithUniqueDigits4(int n) {
        if (n == 0) {
            return 1;
        }

        int cnt = 0;
        for (int i = 0; i <= n; i++) {// Τ碭计
            cnt += getCntByDigit(i);
        }
        return cnt;
    }

    private static int getCntByDigit(int num) {
        if (num == 0) {
            return 1;
        }
        int cnt = 9;// 材计1-9
        int followingPossibleDigitCnt = 9;
        for (int i = 1; i < num; i++) {
            cnt *= followingPossibleDigitCnt--;
        }
        return cnt;
    }

    // 3------------------------------------------------------------------------

    public static int countNumbersWithUniqueDigits3(int n) {
        if (n == 0) {
            return 1;
        }
        long max = (long) Math.pow(10, n);
        // n==3
        // 10
        boolean[] used = new boolean[10];
        int cnt = 10;
        for (int i = 1; i <= 9; i++) {// 材计1-9
            used[i] = true;
            cnt += recursion(i, max, used);
            used[i] = false;
        }
        return cnt;
    }

    private static int recursion(int prev, long max, boolean[] used) {
        int cnt = 0;
        for (int i = 0; i <= 9; i++) {// 材ぇ计0-9
            if (used[i]) {
                continue;
            } else {
                int current = prev * 10 + i;
                if (current < max) {
                    cnt++;
                    used[i] = true;
                    cnt += recursion(current, max, used);
                    used[i] = false;
                }

            }
        }
        return cnt;
    }

    // 2------------------------------------------------------------------------

    // 10,101,102,103,104,105,106,107,108,109
    // 12,120,121,123,124,125,126,127,128,129
    public static int countNumbersWithUniqueDigits(int n) {
        if (n > 10) {
            return countNumbersWithUniqueDigits(10);
        }
        int count = 1; // x == 0
        long max = (long) Math.pow(10, n);

        boolean[] used = new boolean[10];

        for (int i = 1; i < 10; i++) {// 材计1-9
            used[i] = true;
            count += search(i, max, used);
            used[i] = false;
        }

        return count;
    }

    private static int search(long prev, long max, boolean[] used) {
        int count = 0;
        if (prev < max) {
            count += 1;
        } else {
            return count;
        }

        for (int i = 0; i < 10; i++) {// 材ぇ计0-9
            if (!used[i]) {
                used[i] = true;
                long cur = 10 * prev + i;
                count += search(cur, max, used);
                used[i] = false;
            }
        }

        return count;
    }

    // 1------------------------------------------------------------------------
    // ***[dynamic programming]盢ぃ计だ秨矪瞶
    public static int countNumbersWithUniqueDigits2(int n) {
        if (n == 0) {
            return 1;
        }
        if (n == 1) {
            return 10;
        }

        int[] temp = new int[n + 1];
        temp[1] = 10;
        temp[2] = 81;
        int num = 8;
        for (int i = 3; i <= n; i++) {
            temp[i] = temp[i - 1] * num;
            num--;
        }

        int result = 0;
        for (int i = 1; i <= n; i++) {
            result += temp[i];
        }

        return result;

    }

}
