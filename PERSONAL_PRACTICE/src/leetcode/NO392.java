package leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NO392 {
    public static void main(String[] args) {
        // ***[int] max int: 2147483647
        // System.out.println(Integer.MAX_VALUE);

        // System.out.println(isSubsequence("abc", "ahbgdc"));// true
        // System.out.println(isSubsequence("axc", "ahbgdc"));// false
        // System.out.println(isSubsequence("", "ahbgdc"));// false
        // System.out.println("-------------------");
        // System.out.println(isSubsequence2("abc", "ahbgdc"));// true
        // System.out.println(isSubsequence2("axc", "ahbgdc"));// false
        // System.out.println(isSubsequence2("", "ahbgdc"));// false

        // ***[list] search specific item in sort list, and return index
        // int j = Collections.binarySearch(indexList, prev);

        testBinarySearch();
    }

    public static int testBinarySearch() {
        String s = "ahbgdc";
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < s.length(); i++) {
            list.add(s.substring(i, i + 1));
        }
        Collections.sort(list);
        System.out.println(list);
        // ***[list] search item in list, and return index, sort before use it
        System.out.println(Collections.binarySearch(list, "a"));
        System.out.println(Collections.binarySearch(list, "h"));
        System.out.println(Collections.binarySearch(list, "b"));
        System.out.println(Collections.binarySearch(list, "g"));
        System.out.println("-------------------");
        System.out.println(Collections.binarySearch(list, "z"));
        System.out.println(Collections.binarySearch(list, ""));

        return 0;
    }

    // ***[map]重複用將資料先存入map，會比較慢
    public static boolean isSubsequence2(String s, String t) {

        Map<String, List<Integer>> map = new HashMap<String, List<Integer>>();

        for (int i = 0; i < t.length(); i++) {
            String tStr = t.substring(i, i + 1);
            if (!map.containsKey(tStr)) {
                // ***[list]有順序性 ArrayList
                map.put(tStr, new ArrayList<Integer>());
            }
            map.get(tStr).add(i);
        }

        int startIndex = -1;
        // ***[forloop] label for forloop
        outerloop: for (int i = 0; i < s.length(); i++) {
            String sStr = s.substring(i, i + 1);
            if (map.containsKey(sStr)) {
                List<Integer> indexList = map.get(sStr);
                for (Integer num : indexList) {
                    if (startIndex < num) {
                        startIndex = num;
                        // ***[forloop] specify forloop
                        continue outerloop;
                    }
                }
            }
            return false;
        }

        return true;
    }

    public static boolean isSubsequence(String s, String t) {

        int sLen = s.length();
        int index = 0;
        if (sLen == 0) {
            return true;
        }
        for (int i = 0; i < t.length(); i++) {
            char tChar = t.charAt(i);
            char sChar = s.charAt(index);
            if (tChar == sChar) {
                index++;
                if (index == sLen) {
                    return true;
                }
            }
        }
        return false;
    }

}
