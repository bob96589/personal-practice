package goanimate;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class Test01 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
    }

    static void printBracketMatch(String a) {
        char[] charAry = a.toCharArray();
        Stack<Character> stack = new Stack<Character>();

        List<Character> list = Arrays.asList('(', ')', '[', ']', '{', '}', '<', '>');
        for (int i = 0; i < charAry.length; i++) {
            char current = charAry[i];
            if (!list.contains(current)) {
                continue;
            }
            if (current == '(') {
                stack.push(')');
            } else if (current == '[') {
                stack.push(']');
            } else if (current == '{') {
                stack.push('}');
            } else if (current == '<') {
                stack.push('>');
            } else {
                if (stack.isEmpty() || stack.pop() != current) {
                    System.out.println("NO MATCH");
                    return;
                }
            }
        }
        if (stack.isEmpty()) {
            System.out.println("MATCH");
        } else {
            System.out.println("NO MATCH");
        }

    }

}
