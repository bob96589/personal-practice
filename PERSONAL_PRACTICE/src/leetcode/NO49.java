package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//@@@ important
public class NO49 {

    public static void main(String[] args) {
        System.out.println(groupAnagrams(new String[] { "" }));
    }

    public static List<List<String>> groupAnagrams(String[] strs) {
        int len = strs.length;

        // create sorted list
        String[] sortedList = new String[len];
        for (int i = 0; i < len; i++) {
            // @@@[string] sort string, convert to char array first
            char[] charAry = strs[i].toCharArray();
            Arrays.sort(charAry);
            sortedList[i] = String.valueOf(charAry);
        }

        // map with index
        Map<String, List<Integer>> map = new HashMap<String, List<Integer>>();
        for (int i = 0; i < len; i++) {
            if (map.containsKey(sortedList[i])) {
                map.get(sortedList[i]).add(i);
            } else {
                // @@@[list] 建構式中的參數是capacity
                List<Integer> temp = new ArrayList<Integer>();
                temp.add(i);
                map.put(sortedList[i], temp);
            }
        }

        // create resultList
        List<List<String>> resultList = new ArrayList<List<String>>();
        // @@@[map] keySet
        for (String key : map.keySet()) {
            List<Integer> indexList = map.get(key);
            List<String> temp = new ArrayList<String>();
            for (Integer index : indexList) {
                temp.add(strs[index]);
            }
            resultList.add(temp);
        }
        return resultList;
    }

}
