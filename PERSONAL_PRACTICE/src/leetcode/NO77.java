package leetcode;

import java.util.ArrayList;
import java.util.List;

public class NO77 {

    public static void main(String[] args) {
        System.out.println(combine(4, 2));

    }

    public static List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> resultList = new ArrayList<List<Integer>>();
        backTracking(n, k, -1, new ArrayList<Integer>(), resultList);
        return resultList;
    }

    public static void backTracking(int totalCnt, int selectedCnt, int pos, List<Integer> temp, List<List<Integer>> resultList) {
        if (temp.size() == selectedCnt) {
            resultList.add(new ArrayList<Integer>(temp));
            return;
        }
        for (int i = pos + 1; i < totalCnt; i++) {// 每一個值都走走看，通常都是index
            int value = i + 1;
            temp.add(value);
            backTracking(totalCnt, selectedCnt, i, temp, resultList);
            temp.remove(temp.size() - 1);
        }
    }

}
