package leetcode;

public class NO459 {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        // "abab"
        System.out.println(repeatedSubstringPattern3("abab"));

    }

    // 3---------------------------------------------------------------------
    // beat 38%
    public static boolean repeatedSubstringPattern3(String s) {
        int len = s.length();
        for (int i = len / 2; i >= 1; i--) {
            // @@@[substring]找符合倍數的，從中間開始
            if (len % i == 0) {
                int duplicateCnt = len / i;
                String subStr = s.substring(0, i);
                StringBuffer temp = new StringBuffer();
                for (int j = 0; j < duplicateCnt; j++) {
                    temp.append(subStr);
                }
                if (s.equals(temp.toString())) {
                    return true;
                }
            }
        }
        return false;
    }

    // 2---------------------------------------------------------------------
    // beat 40%
    // @@@[substring]找是否有重複字串，將字串串起，去頭去尾，看是否含有字串
    public static boolean repeatedSubstringPattern2(String s) {
        String combinedStr = s + s;
        combinedStr = combinedStr.substring(1, combinedStr.length() - 1);
        return combinedStr.contains(s);
    }

    // 1---------------------------------------------------------------------
    // Time Limit Exceeded
    // 47 / 107 test cases passed.
    public static boolean repeatedSubstringPattern(String s) {
        int len = s.length();
        for (int i = 1; i <= len / 2; i++) {
            String temp = s.replace(s.substring(0, i), "");
            if (temp.isEmpty()) {
                return true;
            }
        }
        return false;
    }

}
