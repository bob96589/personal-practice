package leetcode;

public class NO147 {

    public static void main(String[] args) {
        // 3,2,4

        ListNode head = new ListNode(3);
        head.next = new ListNode(2);
        head.next.next = new ListNode(4);
        insertionSortList3(head);
    }

    // https://discuss.leetcode.com/topic/8570/an-easy-and-clear-way-to-sort-o-1-space
    // TODO
    // unfinished

    // 3----------------------------------------------------------------------

    // use helper
    // prev --> prevNext
    // 在prev就insert，會讓整個程式變簡單

    public static ListNode insertionSortList3(ListNode head) {
        ListNode helper = new ListNode(0);
        while (head != null) {
            ListNode target = head;
            head = head.next;
            ListNode pos = helper;
            while (pos != null) {
                ListNode nextPos = pos.next;
                if (nextPos == null) {
                    target.next = null;
                    pos.next = target;
                } else if (target.val <= nextPos.val) {
                    target.next = nextPos;
                    pos.next = target;
                    break;
                }
                pos = nextPos;
            }
        }
        return helper.next;
    }

    // 2----------------------------------------------------------------------

    // prev --> prevNext
    // 在prev就insert，會讓整個程式變簡單

    public static ListNode insertionSortList2(ListNode head) {
        if (head == null) {
            return head;
        }
        ListNode headA = head;
        ListNode headB = head.next;
        headA.next = null;
        while (headB != null) {
            ListNode target = headB;
            headB = headB.next;
            ListNode tempA = headA;
            if (target.val <= tempA.val) {
                headA = target;
                target.next = tempA;
            } else {
                while (tempA != null) {
                    ListNode nextTempA = tempA.next;
                    if (nextTempA == null) {
                        target.next = null;
                        tempA.next = target;
                    } else if (target.val <= nextTempA.val) {
                        target.next = nextTempA;
                        tempA.next = target;
                        break;
                    }
                    tempA = nextTempA;
                }
            }
        }
        return headA;
    }

    // 1----------------------------------------------------------------------
    // beat 65%
    public ListNode insertionSortList(ListNode head) {
        ListNode aHead = head;
        if (head == null) {
            return head;
        }
        ListNode bHead = head.next;
        aHead.next = null;
        while (bHead != null) {
            ListNode aPreTemp = null;
            ListNode aTemp = aHead;
            ListNode b = bHead;
            bHead = bHead.next;
            b.next = null;
            boolean finished = false;
            while (aTemp != null) {
                if (aTemp.val >= b.val) {
                    if (aPreTemp == null) {
                        aHead = b;
                    } else {
                        aPreTemp.next = b;
                    }
                    b.next = aTemp;
                    finished = true;
                    break;
                } else {
                    aPreTemp = aTemp;
                    aTemp = aTemp.next;
                }
            }
            if (!finished) {
                aPreTemp.next = b;
            }
        }
        return aHead;
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

}
