package algorithm;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * LongestIncreasingSubsequence LONGEST_INCREASING_SEBSEQUENCE http://www.programcreek.com/2014/04/leetcode-longest-increasing-subsequence- java/
 * 
 * @author bob peng
 *
 */
public class LONGEST_INCREASING_SEBSEQUENCE {

    public static int lengthOfLIS_naive(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        // ***[array] fill int array with 1
        int[] max = new int[nums.length];
        Arrays.fill(max, 1);
        int result = 1;
        // i = 0;
        // i = 1; j = 0
        // i = 2; j = 0,1
        // i = 3; j = 0,1,2
        // i = 4; j = 0,1,2,3
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < i; j++) {
                if (nums[i] > nums[j]) {
                    max[i] = Math.max(max[i], max[j] + 1);
                }
            }
            result = Math.max(max[i], result);
        }
        return result;
    }

    public static int lengthOfLIS_binarySearch(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int num : nums) {
            if (list.size() == 0 || num > list.get(list.size() - 1)) {// 直接加入list.add
                list.add(num);
            } else {// 直接覆蓋list.set
                int i = 0;
                int j = list.size() - 1;
                // ***[binary] binarySearch結束點為startIndex >= endIndex
                while (i < j) {
                    int mid = (i + j) / 2;
                    if (list.get(mid) < num) {
                        i = mid + 1;
                    } else {
                        j = mid;
                    }
                }
                list.set(j, num);
            }
        }
        return list.size();
    }

    public static void main(String[] args) {
        int[] intAry = { 1, 12, 7, 0, 23, 11, 52, 31, 61, 69, 70, 2 };
        System.out.println(lengthOfLIS_naive(intAry));
        System.out.println(lengthOfLIS_binarySearch(intAry));
    }

}