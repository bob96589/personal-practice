package leetcode;

public class NO43 {

    public static void main(String[] args) {
        System.out.println(multiply("123", "456"));

    }

    public static void add(int[] result, int pos, int num) {
        int sum = result[pos] + num;
        if (sum <= 9) {
            result[pos] = sum;
        } else {
            int tens = sum / 10;
            int ones = sum % 10;
            result[pos] = ones;
            add(result, pos + 1, tens);
        }
    }

    public static String multiply(String num1, String num2) {

        int[] result = new int[230];

        // reverse string
        // char-'0', convert to int
        // @@@[string] string calculation, reverse string first
        num1 = new StringBuffer(num1).reverse().toString();
        int len1 = num1.length();
        num2 = new StringBuffer(num2).reverse().toString();
        int len2 = num2.length();

        // for loop multiple
        // add to result,
        for (int i = 0; i < len1; i++) {
            for (int j = 0; j < len2; j++) {
                int temp = (num1.charAt(i) - '0') * (num2.charAt(j) - '0');
                // @@@[naming] eng: 個位數ones, 十位數tens
                int ones = temp % 10; // 個位數
                int tens = (temp / 10) % 10; // 十位數
                add(result, i + j, ones);
                add(result, i + j + 1, tens);
            }
        }

        boolean startGetNum = false;
        StringBuffer sb = new StringBuffer();
        for (int i = result.length - 1; i >= 0; i--) {
            if (!startGetNum) {
                if (result[i] == 0) {
                    continue;
                } else {
                    startGetNum = true;
                }
            }
            if (startGetNum) {
                sb.append(result[i]);
            }
        }

        String finalResult = sb.toString();
        return finalResult.isEmpty() ? "0" : finalResult;
    }

}
