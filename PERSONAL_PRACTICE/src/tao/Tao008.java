package tao;

public class Tao008 {
    public static void main(String[] args) {
        int m = 24;
        int n = 36;
        System.out.println("gcd: " + gcd(m, n));
        System.out.println("lcm: " + lcm(m, n));
    }

    public static int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);

    }

    public static int lcm(int c, int d) {
        return c * d / gcd(c, d);

    }
}
