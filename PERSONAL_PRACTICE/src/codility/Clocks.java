package codility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Clocks {

    public static void main(String[] args) {
        int[][] ary = { { 1, 2 }, { 2, 4 }, { 4, 3 }, { 2, 3 }, { 1, 3 } };
        int pos = 4;
        System.out.println(solution(ary, pos));
    }

    public static int solution(int[][] A, int P) {

        int clockCnt = A.length;
        int handCnt = A[0].length;

        List<String> patternList = new ArrayList<String>();
        List<Integer> cntList = new ArrayList<Integer>();

        for (int i = 0; i < clockCnt; i++) {
            int[] hands = A[i];
            List<Integer> list = new ArrayList<Integer>();
            int interval = P - hands[0];
            for (int j = 0; j < handCnt; j++) {
                int temp = (hands[j] + interval) % P;
                list.add(temp == 0 ? P : temp);
            }
            Collections.sort(list);
            StringBuffer sb = new StringBuffer();
            for (int j = 0; j < handCnt; j++) {
                if (j == 0) {
                    sb.append(list.get(j));
                } else {
                    int a = list.get(j) - list.get(j - 1);
                    sb.append("_" + a);
                }
            }
            String result = sb.toString();
            boolean flag = false;
            for (int j = 0; j < patternList.size(); j++) {
                if (patternList.get(j).contains(result)) {
                    flag = true;
                    cntList.set(j, cntList.get(j).intValue() + 1);
                }
            }
            if (!flag) {
                patternList.add(result + "_" + result);
                cntList.add(1);
            }

        }

        int finalResult = 0;
        for (int num : cntList) {
            if (num > 1) {
                finalResult += num * (num - 1) / 2;
            }

        }

        return finalResult;

    }

}
