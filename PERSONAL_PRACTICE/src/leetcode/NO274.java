package leetcode;

import java.util.Arrays;

public class NO274 {
    public static void main(String[] args) {

    }

    // https://discuss.leetcode.com/topic/40765/java-bucket-sort-o-n-solution-with-detail-explanation/2
    // TODO
    public static int hIndex2(int[] citations) {

        int len = citations.length;
        int[] bucket = new int[len + 1];
        for (int c : citations) {
            if (c >= len) {
                bucket[len]++;
            } else {
                bucket[c]++;
            }
        }

        int cnt = 0;
        for (int i = len; i >= 0; i--) {
            cnt += bucket[i];
            if (cnt >= i) {
                return i;
            }
        }
        return 0;
    }

    public static int hIndex(int[] citations) {
        Arrays.sort(citations);
        int len = citations.length;
        int result = 0;
        for (int i = 0; i < citations.length; i++) {
            int paperCnt = len - i;
            int citationsCnt = citations[i];
            result = Math.max(result, Math.min(paperCnt, citationsCnt));
        }
        return result;
    }
}
