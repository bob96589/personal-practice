package leetcode;

//TODO
public class NO26 {

    public static void main(String[] args) {
        System.out.println(removeDuplicates(new int[] { 4, 5, 5, 5, 5, 5 }));

    }

    // ***[array] remove duplicates
    public static int removeDuplicates(int[] nums) {
        int len = nums.length;
        int cnt = 0;
        for (int i = 0; i < len; i++) {
            if (i == 0 || nums[i] != nums[i - 1]) {
                nums[cnt++] = nums[i];
            }
        }
        return cnt;
    }

}
