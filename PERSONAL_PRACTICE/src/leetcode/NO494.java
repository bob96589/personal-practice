package leetcode;

public class NO494 {

    public static void main(String[] args) {
        System.out.println(findTargetSumWays(new int[] { 1, 1, 1, 1, 1 }, 3));

        // 1,2 len=2
        // +1,+2
        // +1,-2
        // -1,+2
        // -1,-2
    }

    // 1-------------------------------------------------------------------------------------
    public static int findTargetSumWays(int[] nums, int S) {
        return dfs(nums, -1, 0, S);
    }

    // ***[dfs]運用在二元樹上，走訪各種可能
    private static int dfs(int[] nums, int index, int sum, int result) {
        if (index == nums.length - 1) {
            if (sum == result) {
                return 1;
            } else {
                return 0;
            }
        }
        int add = dfs(nums, index + 1, sum + nums[index + 1], result);
        int minus = dfs(nums, index + 1, sum - nums[index + 1], result);
        return add + minus;
    }

}
