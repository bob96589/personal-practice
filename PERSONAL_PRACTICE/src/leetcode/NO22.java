package leetcode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class NO22 {

    public static void main(String[] args) {
        System.out.println(generateParenthesis(3));

    }

    public static List<String> generateParenthesis(int n) {
        List<String> result = new ArrayList<String>();
        // @@@[collection] stack is stack, linkedlist is queue
        Stack<String> stack = new Stack<String>();
        stack.add("(");
        backTracking(n - 1, n, new StringBuffer("("), stack, result);
        return result;

    }

    public static void backTracking(int startPCnt, int endPCnt, StringBuffer temp, Stack<String> stack, List<String> result) {

        if (startPCnt == 0 && endPCnt == 0 && stack.isEmpty()) {
            result.add(temp.toString());
        }

        if (startPCnt != 0) {
            temp.append("(");
            stack.push("(");
            backTracking(startPCnt - 1, endPCnt, temp, stack, result);
            // @@@[string] delete char from stringbuffer
            temp.deleteCharAt(temp.length() - 1);
            stack.pop();
        }

        if (endPCnt != 0) {
            if (!stack.isEmpty() && "(".equals(stack.peek())) {
                temp.append(")");
                stack.pop();
                backTracking(startPCnt, endPCnt - 1, temp, stack, result);
                temp.deleteCharAt(temp.length() - 1);
                stack.push("(");
            } else {
                return;
            }
        }

    }

}
