package leetcode;

import java.util.HashMap;
import java.util.Map;

//important
//***[dfs]Construct Binary Tree from Inorder and Postorder Traversal
public class NO106 {

    public static void main(String[] args) {
        buildTree(new int[] { 1, 3, 2 }, new int[] { 3, 2, 1 });
    }

    public static Map<Integer, Integer> map = new HashMap<Integer, Integer>();

    public static TreeNode buildTree(int[] inorder, int[] postorder) {
        if (inorder == null || inorder.length == 0) {
            return null;
        }
        for (int i = 0; i < inorder.length; i++) {
            map.put(inorder[i], i);
        }
        return dfs2(inorder, 0, inorder.length - 1, postorder, 0, postorder.length - 1);
    }

    private static TreeNode dfs2(int[] inorder, int startIndex, int endIndex, int[] postorder, int postStartIndex, int postEndIndex) {
        if (startIndex > endIndex) {
            return null;
        }
        int val = postorder[postEndIndex];
        TreeNode treeNode = new TreeNode(val);

        if (startIndex == endIndex) {
            return treeNode;
        }
        // ***[map]find value by key
        int inorderIndex = map.get(val);
        // int inorderIndex = 0;
        // for (int i = 0; i < inorder.length; i++) {
        // if (inorder[i] == val) {
        // inorderIndex = i;
        // }
        // }
        int nleft = inorderIndex - startIndex;
        treeNode.right = dfs2(inorder, inorderIndex + 1, endIndex, postorder, postStartIndex + nleft, postEndIndex - 1);
        treeNode.left = dfs2(inorder, startIndex, inorderIndex - 1, postorder, postStartIndex, postStartIndex + nleft - 1);
        return treeNode;
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }

        @Override
        public String toString() {
            return Integer.toString(val);
        }

    }
}
