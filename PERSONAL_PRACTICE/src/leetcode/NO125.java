package leetcode;

import java.util.ArrayList;
import java.util.List;

public class NO125 {

    public static void main(String[] args) {
        // System.out.println('A' + 0);
        // System.out.println('a' + 0);
        // System.out.println('z' + 0);
        // System.out.println('0' + 0);
        // System.out.println('9' + 0);
        // 65
        // 97
        // 122
        // 48
        // 57
        System.out.println(isPalindrome2("0P"));

    }

    // 1-------------------------------------------------------------------------
    // beat 39%
    public static boolean isPalindrome2(String s) {
        s = s.toLowerCase();
        char[] charAry = s.toCharArray();
        // @@@[collections] Character in collections
        List<Character> list = new ArrayList<Character>();
        for (char c : charAry) {
            // @@@[char] char calculation, associated with string replace regex
            if ((97 <= c && c <= 122) || (48 <= c && c <= 57)) {
                list.add(c);
            }
        }
        // @@@[array] array�����ۤ�
        int startIndex = 0;
        int endIndex = list.size() - 1;
        while (startIndex < endIndex) {
            if (!list.get(startIndex++).equals(list.get(endIndex--))) {
                return false;
            }
        }
        return true;
    }

    // 1-------------------------------------------------------------------------
    // beat 5%
    public static boolean isPalindrome(String s) {
        s = s.toLowerCase();
        // @@@[string] replace str using regex, slower
        s = s.replaceAll("[^a-z0-9]", "");
        char[] charAry = s.toCharArray();
        int startIndex = 0;
        int endIndex = charAry.length - 1;
        while (startIndex < endIndex) {
            if (charAry[startIndex++] != charAry[endIndex--]) {
                return false;
            }
        }
        return true;
    }

}
