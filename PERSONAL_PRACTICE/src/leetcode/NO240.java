package leetcode;

public class NO240 {

    public static void main(String[] args) {
        // int matrix[][] = new int[][] { { 1, 4, 7, 11, 15 }, { 2, 5, 8, 12, 19
        // }, { 3, 6, 9, 16, 22 }, { 10, 13, 14, 17, 24 }, { 18, 21, 23, 26, 30
        // } };

        int matrix[][] = new int[][] { {} };
        System.out.println(searchMatrix(matrix, 20));

    }

    // ***[d&c] matrix
    public static boolean searchMatrix(int[][] matrix, int target) {
        int iLen = matrix.length;
        if (iLen == 0) {
            return false;
        }
        int jLen = matrix[0].length;
        if (jLen == 0) {
            return false;
        }
        return searchPartOfMatrix(matrix, 0, 0, iLen - 1, jLen - 1, target);
    }

    public static boolean searchPartOfMatrix(int[][] matrix, int topLeftI, int topLeftJ, int downRightI, int downRightJ, int target) {
        int iLen = downRightI - topLeftI + 1;
        if (iLen == 0) {
            return false;
        }
        int jLen = downRightJ - topLeftJ + 1;
        int minLen = Math.min(iLen, jLen);
        int divideI = -1;
        int divideJ = -1;
        if (target < matrix[topLeftI][topLeftJ]) {// less than matrix[0][0]
            return false;
        }
        for (int i = 0; i < minLen; i++) {
            int val = matrix[topLeftI + i][topLeftJ + i];
            if (target == val) {
                return true;
            } else if (val < target) {
                divideI = topLeftI + i;
                divideJ = topLeftJ + i;
            } else {
                break;
            }
        }
        if (divideI + 1 <= downRightI && topLeftJ <= divideJ) {
            boolean partA = searchPartOfMatrix(matrix, divideI + 1, topLeftJ, downRightI, divideJ, target);
            if (partA) {
                return true;
            }
        }
        if (topLeftI <= divideI && divideJ + 1 <= downRightJ) {
            boolean partB = searchPartOfMatrix(matrix, topLeftI, divideJ + 1, divideI, downRightJ, target);
            return partB;
        }
        return false;
    }

}
