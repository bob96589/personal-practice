package algorithm;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

//graph, matrix, binary tree
public class BFS_QUEUE {

    // 6---------------------------------------------------------------------------------------------------
    // graph
    public static void initClosestDistance_bfs(int newFestival, int[] distanceMatrix, List<List<Integer>> adjList) {
        // 2: find and remove item without in
        Queue<Integer> queue = new LinkedList<Integer>();
        int distance = -1;
        queue.add(newFestival);
        while (!queue.isEmpty()) {
            distance++;
            int len = queue.size();
            for (int i = 0; i < len; i++) {
                Integer current = queue.remove();
                distanceMatrix[current] = distance;
                for (Integer next : adjList.get(current)) {// 有向外相鄰,的每一個
                    if (distanceMatrix[next] == -1 || distance < distanceMatrix[next]) {// 無向內相鄰
                        queue.add(next);
                    }
                }
            }
        }
    }

    // 5---------------------------------------------------------------------------------------------------
    // graph
    // beat 60%
    public static boolean canFinish4(int numCourses, int[][] prerequisites) {
        // 1: create inCnt, outArray
        List<Integer> inCnt = new ArrayList<Integer>(numCourses);
        List<List<Integer>> outLists = new ArrayList<List<Integer>>(numCourses);
        for (int i = 0; i < numCourses; i++) {
            inCnt.add(0);
            outLists.add(new ArrayList<Integer>());
        }
        for (int i = 0; i < prerequisites.length; i++) {
            int zero = prerequisites[i][0];
            int one = prerequisites[i][1];
            inCnt.set(zero, inCnt.get(zero).intValue() + 1);
            outLists.get(one).add(zero);
        }

        // 2: find and remove item without in
        Queue<Integer> queue = new LinkedList<Integer>();
        for (int i = 0; i < numCourses; i++) {
            if (inCnt.get(i).equals(0)) {
                queue.add(i);
            }
        }
        while (!queue.isEmpty()) {
            Integer current = queue.remove();

            for (Integer out : outLists.get(current)) {// 有向外相鄰,的每一個
                inCnt.set(out, inCnt.get(out).intValue() - 1);
                if (inCnt.get(out).equals(0)) {// 無向內相鄰
                    queue.add(out);
                }
            }
        }
        // 3: search the existing inList or outList, to see if there is a cycle
        for (int i = 0; i < numCourses; i++) {
            if (!inCnt.get(i).equals(0)) {
                return false;
            }
        }
        return true;
    }

    // 4-----------------------------------------------------------------------------
    // matrix
    // NO417.java：上下左右存在map
    public static void bfs_matrix(int[][] matrix, Queue<int[]> queue, boolean[][] visited) {
        int iLen = matrix.length;
        int jLen = matrix[0].length;
        while (!queue.isEmpty()) {
            int len = queue.size();
            for (int i = 0; i < len; i++) {
                int[] position = queue.poll();
                int iIndex = position[0];
                int jIndex = position[1];
                int currentVal = matrix[iIndex][jIndex];
                int newVal = 0;
                visited[iIndex][jIndex] = true;
                // up
                if (iIndex - 1 >= 0 && !visited[iIndex - 1][jIndex]) {
                    newVal = matrix[iIndex - 1][jIndex];
                    if (currentVal <= newVal) {
                        queue.add(new int[] { iIndex - 1, jIndex });
                    }
                }
                // left
                if (jIndex - 1 >= 0 && !visited[iIndex][jIndex - 1]) {
                    newVal = matrix[iIndex][jIndex - 1];
                    if (currentVal <= newVal) {
                        queue.add(new int[] { iIndex, jIndex - 1 });
                    }
                }
                // down
                if (iIndex + 1 < iLen && !visited[iIndex + 1][jIndex]) {
                    newVal = matrix[iIndex + 1][jIndex];
                    if (currentVal <= newVal) {
                        queue.add(new int[] { iIndex + 1, jIndex });
                    }
                }
                // right
                if (jIndex + 1 < jLen && !visited[iIndex][jIndex + 1]) {
                    newVal = matrix[iIndex][jIndex + 1];
                    if (currentVal <= newVal) {
                        queue.add(new int[] { iIndex, jIndex + 1 });
                    }
                }
            }
        }
    }

    // 3-----------------------------------------------------------------------------
    // binary tree
    // 一個while處理一個level
    public static void levelOrderQueue3(TreeNode root) {
        if (root == null) {
            return;
        }

        LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
        // ***[queue]similar to offer
        queue.add(root);
        int level = 0;
        while (!queue.isEmpty()) {
            level++;
            // ***[algorithm: BFS]何時要跳至下個level
            int len = queue.size();
            for (int i = 0; i < len; i++) {
                // ***[queue]poll取出第一個，並刪除, similar to remove
                TreeNode treeNode = queue.poll();
                TreeNode childLeft = treeNode.left;
                TreeNode childRight = treeNode.right;
                if (childLeft != null) {
                    queue.add(childLeft);
                }
                if (childRight != null) {
                    queue.add(childRight);
                }
            }
        }
    }

    // 2-----------------------------------------------------------------------------
    // binary tree
    public static void levelOrderQueue(TreeNode root) {
        LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
        if (root == null) {
            return;
        }
        queue.add(root);
        while (!queue.isEmpty()) {
            TreeNode n = (TreeNode) queue.remove();
            System.out.print(" " + n.val);
            if (n.left != null) {
                queue.add(n.left);
            }
            if (n.right != null) {
                queue.add(n.right);
            }
        }
    }

    // 1-----------------------------------------------------------------------------
    // binary tree
    public static void levelOrderQueue2(TreeNode root) {
        LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
        if (root == null) {
            return;
        }
        queue.add(root);
        int level = 0;
        while (!queue.isEmpty()) {
            level++;
            int len = queue.size();
            while (len > 0) {
                len--;
                TreeNode n = (TreeNode) queue.remove();
                if (n.left != null) {
                    queue.add(n.left);
                }
                if (n.right != null) {
                    queue.add(n.right);
                }
            }
        }
    }

    public static void main(String[] args) throws java.lang.Exception {
        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(10);
        root.right = new TreeNode(15);
        root.left.left = new TreeNode(20);
        root.left.right = new TreeNode(25);
        root.right.left = new TreeNode(30);
        root.right.right = new TreeNode(35);

        System.out.println("Breadth First Search : ");
        levelOrderQueue(root);
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int val) {
            this.val = val;
        }
    }
}
