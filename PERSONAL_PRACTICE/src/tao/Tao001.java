package tao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Tao001 {
    public static void main(String[] args) {
        // ***tao: ctrl + f11: run as java application shortcut
        List<Integer> list = new ArrayList<Integer>();
        for (String s : args) {
            list.add(Integer.parseInt(s));
        }
        Collections.sort(list);
        Collections.sort(list, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        });
        System.out.println(list);
        System.out.println(list.get(5));
    }
}
