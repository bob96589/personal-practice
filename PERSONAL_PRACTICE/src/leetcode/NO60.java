package leetcode;

import java.util.ArrayList;
import java.util.List;

public class NO60 {

    // The set [1,2,3,…,n] contains a total of n! unique permutations.
    //
    // By listing and labeling all of the permutations in order,
    // We get the following sequence (ie, for n = 3):
    //
    // "123"
    // "132"
    // "213"
    // "231"
    // "312"
    // "321"
    // Given n and k, return the kth permutation sequence.
    //
    // Note: Given n will be between 1 and 9 inclusive.

    public static void main(String[] args) {
        System.out.println(getPermutation(3, 4));

    }

    public static String getPermutation(int n, int k) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        backTracking(n, k, 0, new boolean[n + 1], new ArrayList<Integer>(), result);
        List<Integer> list = result.get(result.size() - 1);
        StringBuffer sb = new StringBuffer();
        for (Integer i : list) {
            sb.append(i);
        }
        return sb.toString();

    }

    public static void backTracking(int n, int k, int pos, boolean[] visited, List<Integer> temp, List<List<Integer>> result) {
        if (pos == n) {
            result.add(new ArrayList<Integer>(temp));
        }
        if (k == result.size()) {
            return;
        }
        for (int i = 1; i <= n; i++) {// 每一個值都走走看
            if (visited[i]) {
                continue;
            }
            visited[i] = true;
            temp.add(i);
            backTracking(n, k, pos + 1, visited, temp, result);
            visited[i] = false;
            temp.remove(temp.size() - 1);
        }
    }

}
