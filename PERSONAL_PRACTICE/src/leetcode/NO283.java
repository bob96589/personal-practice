package leetcode;

//TODO
public class NO283 {

    public static void main(String[] args) {

    }

    public static void moveZeroes(int[] nums) {

        int index = 0;
        int length = nums.length;

        for (int num : nums) {
            if (num != 0) {
                nums[index] = num;
                index++;
            }
        }
        while (index < length) {
            nums[index] = 0;
            index++;
        }

    }
}
