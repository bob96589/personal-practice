package hackerrank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

//unacceptable answer
public class Sherlock_and_Cost_2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            int k = in.nextInt();
            int[] ary = new int[k];
            for (int j = 0; j < k; j++) {
                ary[j] = in.nextInt();
            }
            subsets(ary);
        }

        // subsets(new int[]{10 ,1, 10, 1, 10});
        // subsets(new int[] { 100, 2, 100, 2, 100 });
    }

    public static void subsets(int[] nums) {
        List<Integer> totalScoreList = new ArrayList<Integer>();
        backTracking(nums, -1, 0, 0, totalScoreList);
        Collections.sort(totalScoreList);
        System.out.println(totalScoreList.get(totalScoreList.size() - 1));
    }

    // ***[backtracking]
    public static void backTracking(int[] nums, int pos, int currScore, int previous, List<Integer> totalScoreList) {
        if (pos == nums.length - 1) {
            totalScoreList.add(currScore);
            return;
        }
        int newPos = pos + 1;

        if (newPos == 0) {
            backTracking(nums, newPos, 0, nums[newPos], totalScoreList);
            if (nums[newPos] != 1) {
                backTracking(nums, newPos, 0, 1, totalScoreList);
            }
        } else {
            backTracking(nums, newPos, currScore + getScore(nums[newPos], previous), nums[newPos], totalScoreList);
            if (nums[newPos] != 1) {
                backTracking(nums, newPos, currScore + getScore(1, previous), 1, totalScoreList);
            }
        }
    }

    public static int getScore(int a, int b) {
        return a > b ? a - b : b - a;
    }

}
