package leetcode;

import java.util.ArrayList;
import java.util.List;

public class NO78 {
    // Given a set of distinct integers, nums, return all possible subsets.
    // Note: The solution set must not contain duplicate subsets.
    // For example,
    // If nums = [1,2,3], a solution is:
    // [
    // [3],
    // [1],
    // [2],
    // [1,2,3],
    // [1,3],
    // [2,3],
    // [1,2],
    // []
    // ]
    public static void main(String[] args) {
        System.out.println(subsets(new int[] { 1, 2, 3 }));

    }

    public static List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> resultList = new ArrayList<List<Integer>>();
        backTracking(nums, -1, new ArrayList<Integer>(), resultList);
        return resultList;
    }

    // ***[backtracking]
    public static void backTracking(int[] nums, int pos, List<Integer> temp, List<List<Integer>> resultList) {
        // ***[backtracking]複製List
        resultList.add(new ArrayList<Integer>(temp));
        for (int i = pos + 1; i < nums.length; i++) {// 每一個值都走走看
            temp.add(nums[i]);
            backTracking(nums, i, temp, resultList);
            temp.remove(temp.size() - 1);
        }
    }

}
