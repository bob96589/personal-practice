package leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

//ok
public class NO111 {

    public static void main(String[] args) {
        TreeNode t1 = new TreeNode(1);
        TreeNode t2 = new TreeNode(2);
        TreeNode t3 = new TreeNode(3);
        TreeNode t4 = new TreeNode(4);
        TreeNode t5 = new TreeNode(5);
        t1.left = t2;
        t1.right = t3;
        t2.left = t4;
        t2.right = t5;
        System.out.println(minDepth5(t1));
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

    public static int minDepth5(TreeNode root) {
        if (root == null) {
            return 0;
        }

        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.add(root);
        int level = 0;
        while (true) {
            level++;
            int len = queue.size();
            for (int i = 0; i < len; i++) {
                TreeNode treeNode = queue.poll();
                TreeNode childLeft = treeNode.left;
                TreeNode childRight = treeNode.right;
                if (childLeft == null && childRight == null) {
                    return level;
                }
                if (childLeft != null) {
                    queue.add(childLeft);
                }
                if (childRight != null) {
                    queue.add(childRight);
                }
            }
        }
    }

    public static int minDepth4(TreeNode root) {
        if (root == null) {
            return 0;
        }

        List<TreeNode> currentList = new ArrayList<TreeNode>();
        currentList.add(root);
        List<TreeNode> nextList = new ArrayList<TreeNode>();
        int level = 0;
        while (true) {
            level++;
            for (TreeNode treeNode : currentList) {
                TreeNode childLeft = treeNode.left;
                TreeNode childRight = treeNode.right;
                if (childLeft == null && childRight == null) {
                    return level;
                }
                if (childLeft != null) {
                    nextList.add(childLeft);
                }
                if (childRight != null) {
                    nextList.add(childRight);
                }
            }
            // currentList = nextList;
            // nextList = new ArrayList<TreeNode>();
            currentList.clear();
            currentList.addAll(nextList);
            nextList.clear();
        }
    }

    public static int minDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int min;
        if (root.left != null && root.right != null) {
            min = Math.min(minDepth(root.left), minDepth(root.right)) + 1;
        } else if (root.left != null) {
            min = minDepth(root.left) + 1;
        } else if (root.right != null) {
            min = minDepth(root.right) + 1;
        } else {
            min = 1;
        }
        return min;
    }

    public static int minDepth3(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return getMinDepth(root, 1);

    }

    public static int getMinDepth(TreeNode root, int depth) {
        if (root.left == null && root.right == null) {
            return depth;
        }

        int miniDepth = -1;
        if (root.left != null) {
            int leftDepth = getMinDepth(root.left, depth + 1);
            if (miniDepth == -1 || miniDepth > leftDepth) {
                miniDepth = leftDepth;
            }
        }
        if (root.right != null) {
            int rightDepth = getMinDepth(root.right, depth + 1);
            if (miniDepth == -1 || miniDepth > rightDepth) {
                miniDepth = rightDepth;
            }
        }

        return miniDepth;

    }
}
