package leetcode;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class NO20 {

    public static void main(String[] args) {
        System.out.println(isValid("()[]{}"));
    }

    // 2------------------------------------------------------------------------------
    public static boolean isValid2(String s) {
        char[] charAry = s.toCharArray();
        Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < charAry.length; i++) {
            char current = charAry[i];
            if (current == '(') {
                stack.push(')');
            } else if (current == '[') {
                stack.push(']');
            } else if (current == '{') {
                stack.push('}');
            } else {
                if (stack.isEmpty() || stack.pop() != current) {
                    return false;
                }
            }
        }
        if (stack.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    // 1------------------------------------------------------------------------------
    public static boolean isValid(String s) {
        char[] charAry = s.toCharArray();
        // @@@[list] initialize list, like an array
        List<Character> startBracketlist = Arrays.asList('(', '[', '{');
        // @@@[collection] char in collections
        Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < charAry.length; i++) {
            char current = charAry[i];
            if (startBracketlist.contains(current)) {
                stack.add(current);
            } else {
                if (stack.isEmpty()) {
                    return false;
                }
                char top = stack.peek();
                if (top == '(') {
                    if (current == ')') {
                        stack.pop();
                    } else {
                        return false;
                    }
                }
                if (top == '[') {
                    if (current == ']') {
                        stack.pop();
                    } else {
                        return false;
                    }
                }
                if (top == '{') {
                    if (current == '}') {
                        stack.pop();
                    } else {
                        return false;
                    }
                }
            }
        }
        if (stack.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

}
