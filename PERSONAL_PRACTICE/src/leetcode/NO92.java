package leetcode;

//TODO
public class NO92 {

    public static void main(String[] args) {
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        node1.next = node2;
        node2.next = null;

        ListNode node = reverseBetween(node1, 1, 2);

        while (node != null) {
            System.out.println(node.val);
            node = node.next;
        }

    }

    public static ListNode reverseBetween(ListNode head, int m, int n) {
        if (head == null) {
            return head;
        }
        if (head.next == null) {
            return head;
        }
        if (m == n) {
            return head;
        }

        ListNode current = head;

        ListNode m_pre = null;
        ListNode m_curr = null;
        ListNode n_curr = null;
        ListNode n_next = null;

        int position = 1;
        while (current != null) {
            if (position == m - 1) {
                m_pre = current;
            } else if (position == m) {
                m_curr = current;
            }
            if (position == n) {
                n_curr = current;
            } else if (position == n + 1) {
                n_next = current;
                break;
            }
            position++;
            current = current.next;
        }

        n_curr.next = null;
        reverseList(m_curr);
        if (m_pre != null)
            m_pre.next = n_curr;
        m_curr.next = n_next;

        return m_curr.equals(head) ? n_curr : head;
    }

    public static ListNode reverseList(ListNode head) {
        ListNode prev = null;
        ListNode current = head;
        while (current != null) {
            ListNode next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return prev;
    }

    public static ListNode reverseBetweenXX(ListNode head, int m, int n) {

        if (head == null) {
            return head;
        }
        if (head.next == null) {
            return head;
        }
        if (m == n) {
            return head;
        }

        ListNode current = head;

        ListNode m_pre = null;
        ListNode m_curr = null;
        ListNode m_next = null;
        ListNode n_pre = null;
        ListNode n_curr = null;
        ListNode n_next = null;

        int position = 1;
        while (current != null) {

            if (position == m - 1) {
                m_pre = current;
            } else if (position == m) {
                m_curr = current;
            } else if (position == m + 1) {
                m_next = current;
            }

            if (position == n - 1) {
                n_pre = current;
            } else if (position == n) {
                n_curr = current;
            } else if (position == n + 1) {
                n_next = current;
                break;
            }
            position++;
            current = current.next;
        }

        if (m_next.equals(n_curr) && m_curr.equals(n_pre)) {
            if (m_pre != null) {
                m_pre.next = n_curr;
            }
            m_curr.next = n_next;
            n_curr.next = m_curr;
        } else {
            if (m_pre != null) {
                m_pre.next = n_curr;
            }
            m_curr.next = n_next;
            n_pre.next = m_curr;
            n_curr.next = m_next;
        }

        return m_curr.equals(head) ? n_curr : head;

    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }
}
