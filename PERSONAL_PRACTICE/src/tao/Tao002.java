package tao;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Tao002 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        List<Integer> list = new ArrayList<Integer>();
        int cnt = 43;
        int targetCnt = cnt / 10;
        for (int i = 0; i < cnt; i++) {
            list.add(in.nextInt());
        }
        list.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        });
        double d = 0;
        for (int i = 0; i < targetCnt; i++) {
            d += list.get(i);
        }
        System.out.println(list);
        double result = d / targetCnt;
        DecimalFormat dFormat = new DecimalFormat("##.#");
        dFormat.format(result);
        System.out.println(dFormat.format(result));
    }
}

// 57
// 57
// 46
// 32
// 96
// 16
// 25
// 12
// 39
// 38
// 93
// 30
// 62
// 13
// 55
// 75
// 27
// 86
// 10
// 20
// 4
// 49
// 61
// 83
// 27
// 47
// 44
// 57
// 64
// 4
// 37
// 14
// 34
// 89
// 59
// 18
// 7
// 86
// 100
// 31
// 98
// 17
// 28
