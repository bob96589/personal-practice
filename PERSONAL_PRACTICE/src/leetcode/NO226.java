package leetcode;

//ok
public class NO226 {

    public static void main(String[] args) {

    }

    public static TreeNode invertTree(TreeNode root) {
        return invertLeftRight(root);
    }

    // ***[recursion]
    public static TreeNode invertLeftRight(TreeNode root) {
        if (root == null) {
            return null;
        }

        TreeNode left = root.left;
        TreeNode right = root.right;
        root.left = invertLeftRight(right);
        root.right = invertLeftRight(left);
        return root;
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

}
