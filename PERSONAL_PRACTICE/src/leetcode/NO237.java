package leetcode;

//ok
public class NO237 {

    public static void main(String[] args) {

    }

    // ***[linkedlist]找不到上一個，就找下一個
    public static void deleteNode(ListNode node) {
        if (node == null || node.next == null)
            return;
        ListNode next = node.next;
        node.val = next.val;
        node.next = next.next;
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }
}
