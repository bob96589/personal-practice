package leetcode;

public class NO268 {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

    public static int missingNumber(int[] nums) {
        int len = nums.length;
        int totalSum = len * (len + 1) / 2;
        int sumOfArray = 0;
        for (int i = 0; i < len; i++) {
            sumOfArray += nums[i];
        }
        // @@@[int] 先加總，再減去不要的
        return totalSum - sumOfArray;
    }

}
