package leetcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

//ok
public class NO217 {

    public static void main(String[] args) {

    }

    public static boolean containsDuplicate(int[] nums) {
        int length = nums.length;
        if (length == 0) {
            return false;
        }
        // ***[int] int array sort，此方法快很多，先sort在處理
        Arrays.sort(nums);
        int preNum = nums[0];
        for (int i = 1; i < length; i++) {
            if (nums[i] == preNum) {
                return true;
            } else {
                preNum = nums[i];
            }
        }
        return false;
    }

    public static boolean containsDuplicate2(int[] nums) {
        // ***[collection]泛型僅能放物件
        Set<Integer> set = new HashSet<Integer>();
        for (int num : nums) {
            // ***[collection]contains對基本資料型態也有效
            if (set.contains(num)) {
                return true;
            } else {
                set.add(num);
            }
        }
        return false;
    }

}
