package leetcode;

public class NO38 {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

    public static String countAndSay(int n) {
        String s = "1";
        for (int i = 2; i <= n; i++) {// nth
            int len = s.length();
            StringBuffer sb = new StringBuffer();
            int cnt = 0;
            for (int j = 0; j < len; j++) {// go through the string
                // @@@[string]compare prev, item 0 don't do the comparison
                if (j == 0) {
                    cnt++;
                } else {
                    if (s.charAt(j) == s.charAt(j - 1)) {
                        cnt++;
                    } else {
                        sb.append(cnt);
                        sb.append(s.charAt(j - 1));
                        cnt = 1;
                    }
                }
            }
            if (cnt != 0) {
                sb.append(cnt);
                sb.append(s.charAt(len - 1));
            }
            s = sb.toString();
        }
        return s;
    }

}
