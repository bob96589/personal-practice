package leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

//ok
public class NO169 {

    public static void main(String[] args) {

    }

    public static int majorityElement(int[] nums) {
        // ***[array] sort
        Arrays.sort(nums);
        return nums[nums.length / 2];
    }

    public static int majorityElement2(int[] nums) {
        int cnt = 0;
        int majorInt = 0;
        for (int num : nums) {
            if (cnt == 0) {
                majorInt = num;
            }
            if (majorInt == num) {
                cnt++;
            } else {
                cnt--;
            }
        }
        return majorInt;
    }

    public static int majorityElement3(int[] nums) {
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (Integer num : nums) {
            if (map.containsKey(num)) {
                // ***[integer] integer to int
                int val = map.get(num).intValue() + 1;
                map.put(num, val);
            } else {
                map.put(num, 1);
            }
            if (map.containsKey(num) && map.get(num) > nums.length / 2) {
                return num;
            }
        }
        return -1;
    }

}
