package tao;

public class Tao011 {
    public static void main(String[] args) {
        int[] nums = new int[] { 671981, 198132, 362535, 196757, 695719, 405874, 509515, 738467, 479245, 136360, 901789, 524643, 227894, 86332, 285380, 952362, 799208, 339068, 655811, 190561, 777958,
                512406 };

        double total = 0;
        for (int i = 0; i < nums.length; i++) {
            total += nums[i];
        }

        double avg = total / nums.length;

        double result = Math.round(avg * 10);
        result = result / 10;
        System.out.println(result);

        System.out.printf("%.1f", avg);

    }
}
