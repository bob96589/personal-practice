package leetcode;

public class NO371 {

    // 位元運算子(Bitwise operator)
    // op1 & op2
    // op1 | op2
    // op1 ^ op2
    // ~op1
    // 取x的負值，~x+1 = -x
    // http://goodideascome.blogspot.tw/2011/08/2.html
    // ---------------------------------------------------
    // 位移運算子(Shift operator)
    // op1 << op2 將 op1 的位元左移 op2 個單位,右邊補上 0
    // op1 >> op2 將 op1 的位元右移 op2 個單位,左邊補上原來最左邊的位元值
    // op1 >>> op2 將 op1 的位元右移 op2 個單位,左邊補上 0
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

    // @@@[bit manipulation] sum
    public static int getSum(int a, int b) {
        while (b != 0) {
            int carry = a & b;
            a = a ^ b;
            b = carry << 1;
        }
        return a;
    }

    // @@@[bit manipulation] subtract
    public int getSubtract(int a, int b) {
        while (b != 0) {
            int borrow = (~a) & b;
            a = a ^ b;
            b = borrow << 1;
        }
        return a;
    }

}
