package mindcraft;

import java.util.ArrayList;
import java.util.List;

public class NO01 {
    public static void main(String[] args) {
        String[][] inputAry = new String[][] { { "A", "B" }, { "1" }, { "��", "�A" } };
        printResult(inputAry);
    }

    public static void printResult(String[][] inputAry) {
        backTracking(inputAry, 0, new ArrayList<String>());
    }

    public static void backTracking(String[][] inputAry, int startIndex, List<String> temp) {
        if (temp.size() == inputAry.length) {
            System.out.println(temp);
            return;
        }
        String[] innerAry = inputAry[startIndex];
        for (int i = 0; i < innerAry.length; i++) {
            temp.add(innerAry[i]);
            backTracking(inputAry, startIndex + 1, temp);
            temp.remove(temp.size() - 1);
        }
    }
}
