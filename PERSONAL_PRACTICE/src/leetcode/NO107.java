package leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class NO107 {

    public static void main(String[] args) {
        TreeNode t1 = new TreeNode(1);
        TreeNode t2 = new TreeNode(2);
        TreeNode t3 = new TreeNode(3);
        TreeNode t4 = new TreeNode(4);
        t1.left = t2;
        t1.left.left = t3;
        t1.right = t4;
        System.out.println(levelOrderBottom(t1));
    }

    public static List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> resultList = new ArrayList<List<Integer>>();
        if (root == null) {
            return resultList;
        }
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.add(root);
        int level = 0;
        while (!queue.isEmpty()) {
            level++;
            int len = queue.size();
            ArrayList<Integer> levelList = new ArrayList<Integer>();
            for (int i = 0; i < len; i++) {
                TreeNode treeNode = queue.poll();
                levelList.add(treeNode.val);
                TreeNode childLeft = treeNode.left;
                TreeNode childRight = treeNode.right;
                if (childLeft != null) {
                    queue.add(childLeft);
                }
                if (childRight != null) {
                    queue.add(childRight);
                }
            }
            // ***[list] prepend data to list
            resultList.add(0, levelList);
        }
        return resultList;
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

}
