package leetcode;

public class NO70 {

    public static void main(String[] args) {
        // System.out.println(climbStairs(44));
        System.out.println(climbStairs2(44));
    }

    // ***[dynamic programming] 應該用DP，卻沒用，超慢的
    public static int climbStairs(int n) {
        if (n == 1 || n == 2) {
            return n;
        }
        int result = climbStairs(n - 1) + climbStairs(n - 2);
        return result;
    }

    // ***[dynamic programming] standard, bottom up
    public static int climbStairs2(int n) {
        if (n == 1 || n == 2) {
            return n;
        }
        int[] temp = new int[n + 1];
        temp[1] = 1;
        temp[2] = 2;
        for (int i = 3; i <= n; i++) {
            temp[i] = temp[i - 1] + temp[i - 2];
        }
        return temp[n];
    }

}
