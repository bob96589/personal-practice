package hackerrank;

import java.util.Scanner;

public class Java_Stdin_and_Stdout_I {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (scan.hasNext()) {
            int a = scan.nextInt();
            System.out.println(a);
        }
    }

}
