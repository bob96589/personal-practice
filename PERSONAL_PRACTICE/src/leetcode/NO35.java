package leetcode;

//TODO
public class NO35 {

    public static void main(String[] args) {
        // System.out.println((-1)/2);
        System.out.println(searchInsert(new int[] { 1, 3 }, 0));
        System.out.println(searchInsert(new int[] { 1, 3, 5, 6 }, 5));
        System.out.println(searchInsert(new int[] { 1, 3, 5, 6 }, 2));
        System.out.println(searchInsert(new int[] { 1, 3, 5, 6 }, 7));
        System.out.println(searchInsert(new int[] { 1, 3, 5, 6 }, 0));

        System.out.println("-----------------");

        System.out.println(searchInsert2(new int[] { 1, 3 }, 0));
        System.out.println(searchInsert2(new int[] { 1, 3, 5, 6 }, 5));
        System.out.println(searchInsert2(new int[] { 1, 3, 5, 6 }, 2));
        System.out.println(searchInsert2(new int[] { 1, 3, 5, 6 }, 7));
        System.out.println(searchInsert2(new int[] { 1, 3, 5, 6 }, 0));
    }

    public static int searchInsert(int[] nums, int target) {
        return search(nums, 0, nums.length - 1, target);

    }

    public static int searchInsert2(int[] nums, int target) {
        int startIndex = 0;
        int endIndex = nums.length - 1;

        // ***[array] 結束點為startIndex > endIndex
        while (startIndex <= endIndex) {
            int mid = (startIndex + endIndex) / 2;
            if (nums[mid] == target) {
                return mid;
            } else if (target < nums[mid]) {
                endIndex = mid - 1;
            } else {// nums[mid] < target
                startIndex = mid + 1;
            }
        }
        return startIndex;
    }

    // ***[algorithm: recursion]
    private static int search(int[] nums, int startIndex, int endIndex, int target) {
        int mid = (startIndex + endIndex) / 2;
        // ***[array] 結束點為startIndex >= endIndex
        if (startIndex > endIndex) {
            return startIndex;
        } else {
            if (target < nums[mid]) {
                // ***[array] 用過的就不要再用了
                return search(nums, startIndex, mid - 1, target);
            } else if (nums[mid] < target) {
                return search(nums, mid + 1, endIndex, target);
            } else { // (nums[mid] == target)
                return mid;
            }
        }
    }
}
