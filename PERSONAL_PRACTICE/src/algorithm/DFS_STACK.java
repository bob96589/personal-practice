package algorithm;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

// pre-order, �Ȩ��X
//graph dfs
public class DFS_STACK {

    // Use a stack for the iterative DFS version
    public static void dfs_iterative(ArrayList<ArrayList<Integer>> adjList, int startItem) {

        List<Integer> preOrder = new ArrayList<Integer>();

        boolean[] visited = new boolean[adjList.size()];
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(startItem);
        while (!stack.isEmpty()) {
            int topItem = stack.pop();
            if (!visited[topItem]) {
                visited[topItem] = true;
                preOrder.add(topItem);
                System.out.println("item visited: " + topItem);
                Stack<Integer> temp = new Stack<Integer>();
                for (int adjItem : adjList.get(topItem)) {
                    if (!visited[adjItem]) {
                        temp.push(adjItem);
                    }
                }
                // keep the same order in adjacency list
                while (!temp.isEmpty()) {
                    stack.push(temp.pop());
                }
            }
        }
        System.out.println("Pre Order Traversal: " + preOrder.toString());
    }

    // ----------------------------------------------------------------------
    // Testing our implementation
    public static void main(String[] args) {
        ArrayList<ArrayList<Integer>> adjLists = new ArrayList<ArrayList<Integer>>();
        final int n = 7;
        for (int v = 0; v < n; v++) {
            adjLists.add(new ArrayList<Integer>());
        }
        adjLists.get(0).add(1);
        adjLists.get(0).add(2);
        adjLists.get(0).add(3);
        adjLists.get(1).add(5);
        adjLists.get(1).add(6);
        adjLists.get(2).add(4);
        adjLists.get(3).add(2);
        adjLists.get(3).add(4);
        adjLists.get(4).add(1);
        adjLists.get(6).add(4);
        dfs_iterative(adjLists, 0);
    }
}
