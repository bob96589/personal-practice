package leetcode;

import java.util.Arrays;

public class NO189 {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

    public static void rotate(int[] nums, int k) {

        int len = nums.length;
        int b1[] = Arrays.copyOfRange(nums, len - k, len);
        int b2[] = Arrays.copyOfRange(nums, 0, len - k);

        for (int i = 0; i < k; i++) {
            nums[i] = b1[i];
        }
        for (int i = k; i < len; i++) {
            nums[i] = b2[i - k];
        }
    }

}
