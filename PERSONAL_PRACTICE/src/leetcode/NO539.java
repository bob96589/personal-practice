package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NO539 {

    public static void main(String[] args) {
        System.out.println(findMinDifference(Arrays.asList("23:59", "00:00")));
    }

    public static int findMinDifference(List<String> timePoints) {
        Collections.sort(timePoints);

        List<Integer> minAry = new ArrayList<Integer>();
        for (String str : timePoints) {
            int hour = Integer.parseInt(str.substring(0, str.indexOf(":")));
            int min = Integer.parseInt(str.substring(str.indexOf(":") + 1));
            minAry.add(hour * 60 + min);
        }
        int minMin = Integer.MAX_VALUE;
        for (int i = 0; i < minAry.size(); i++) {
            int temp;
            if (i + 1 == minAry.size()) {
                temp = minAry.get(0).intValue() + 60 * 24 - minAry.get(i).intValue();
            } else {
                temp = minAry.get(i + 1).intValue() - minAry.get(i).intValue();
            }
            minMin = Math.min(minMin, temp);
        }
        return minMin;

    }

}
