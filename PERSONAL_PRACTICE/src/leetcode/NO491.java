package leetcode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//***important
public class NO491 {

    public static void main(String[] args) {
        System.out.println(findSubsequences(new int[] { 1, 3, 2, 5, 4 }));
        System.out.println(findSubsequences2(new int[] { 1, 3, 2, 5, 4 }));

    }

    // 2---------------------------------------------------------------------------------
    public static List<List<Integer>> findSubsequences2(int[] nums) {
        // ***[set]不重複
        Set<List<Integer>> result = new HashSet<List<Integer>>();
        List<Integer> temp = new ArrayList<Integer>();
        dfs2(nums, 0, result, temp);
        List a = new ArrayList(result);
        return a;
    }

    // ***[dfs]走訪陣列
    private static void dfs2(int[] nums, int index, Set<List<Integer>> result, List<Integer> temp) {
        if (temp.size() >= 2) {
            // ***[list]複製list
            result.add(new ArrayList(temp));
        }
        // ***[for]小心i, index會混淆
        for (int i = index; i < nums.length; i++) {
            if (temp.size() == 0 || temp.get(temp.size() - 1) <= nums[i]) {
                // ***[dfs]preOrder做的事，在postOrder要還原
                temp.add(nums[i]);
                dfs2(nums, i + 1, result, temp);
                temp.remove(temp.size() - 1);
            }
        }

    }

    // 1---------------------------------------------------------------------------------
    public static List<List<Integer>> findSubsequences(int[] nums) {
        Set<List<Integer>> res = new HashSet<List<Integer>>();
        List<Integer> holder = new ArrayList<Integer>();
        findSequence(res, holder, 0, nums);
        List result = new ArrayList(res);
        return result;
    }

    public static void findSequence(Set<List<Integer>> res, List<Integer> holder, int index, int[] nums) {
        if (holder.size() >= 2) {
            res.add(new ArrayList(holder));
        }
        for (int i = index; i < nums.length; i++) {
            if (holder.size() == 0 || holder.get(holder.size() - 1) <= nums[i]) {
                holder.add(nums[i]);
                findSequence(res, holder, i + 1, nums);
                holder.remove(holder.size() - 1);
            }
        }
    }

}
