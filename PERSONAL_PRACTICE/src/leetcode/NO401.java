package leetcode;

import java.util.ArrayList;
import java.util.List;

public class NO401 {

    public static void main(String[] args) {
        System.out.println(readBinaryWatch(3));
        System.out.println(readBinaryWatch(3).size());
        System.out.println(readBinaryWatch2(3));
        System.out.println(readBinaryWatch2(3).size());
    }

    public static List<String> readBinaryWatch2(int num) {
        List<String> result = new ArrayList<String>();
        int[] nums1 = new int[] { 8, 4, 2, 1 };
        int[] nums2 = new int[] { 32, 16, 8, 4, 2, 1 };
        for (int i = 0; i <= num; i++) {
            List<Integer> list1 = new ArrayList<Integer>();
            List<Integer> list2 = new ArrayList<Integer>();
            backTracking(nums1, i, 0, 0, list1);
            backTracking(nums2, num - i, 0, 0, list2);
            for (int item1 : list1) {
                if (item1 > 11) {
                    continue;
                }
                for (int item2 : list2) {
                    if (item2 > 59) {
                        continue;
                    }
                    result.add(item1 + ":" + (item2 < 10 ? "0" + item2 : item2));
                }
            }
        }
        return result;
    }

    // ***[backtracking]
    public static void backTracking(int[] nums, int cnt, int pos, int sum, List<Integer> list) {
        if (cnt == 0) {
            list.add(sum);
            return;
        }
        for (int i = pos; i < nums.length; i++) {// 每一個值都走走看
            backTracking(nums, cnt - 1, i + 1, sum + nums[i], list);
        }
    }

    // 1---------------------------------------------------------------------------
    public static List<String> readBinaryWatch(int num) {
        List<String> res = new ArrayList<>();
        int[] nums1 = new int[] { 8, 4, 2, 1 }, nums2 = new int[] { 32, 16, 8, 4, 2, 1 };
        for (int i = 0; i <= num; i++) {
            List<Integer> list1 = generateDigit(nums1, i);
            List<Integer> list2 = generateDigit(nums2, num - i);
            for (int num1 : list1) {
                if (num1 >= 12)
                    continue;
                for (int num2 : list2) {
                    if (num2 >= 60)
                        continue;
                    res.add(num1 + ":" + (num2 < 10 ? "0" + num2 : num2));
                }
            }
        }
        return res;
    }

    private static List<Integer> generateDigit(int[] nums, int count) {
        List<Integer> res = new ArrayList<>();
        generateDigitHelper(nums, count, 0, 0, res);
        return res;
    }

    private static void generateDigitHelper(int[] nums, int count, int pos, int sum, List<Integer> res) {
        if (count == 0) {
            res.add(sum);
            return;
        }

        for (int i = pos; i < nums.length; i++) {
            generateDigitHelper(nums, count - 1, i + 1, sum + nums[i], res);
        }
    }
}
