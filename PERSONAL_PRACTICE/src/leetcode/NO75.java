package leetcode;

import java.util.Arrays;

public class NO75 {

    public static void main(String[] args) {
        sortColors3(new int[] { 0, 2, 1, 2, 0, 1 });

    }

    public static void sortColors3(int[] nums) {
        int len = nums.length;
        int zeroIndex = 0;
        int twoIndex = len - 1;
        for (int i = 0; i <= twoIndex; i++) {
            while (nums[i] == 2 && i < twoIndex) {
                int temp = nums[twoIndex];
                nums[twoIndex] = nums[i];
                nums[i] = temp;
                twoIndex--;
            }
            while (nums[i] == 0 && zeroIndex < i) {
                int temp = nums[zeroIndex];
                nums[zeroIndex] = nums[i];
                nums[i] = temp;
                zeroIndex++;
            }
        }
        System.out.println(Arrays.toString(nums));
    }

    public static void sortColors2(int[] nums) {
        int len = nums.length;
        int zeroIndex = 0;
        int twoIndex = len - 1;
        for (int i = 0; i <= twoIndex; i++) {
            if (nums[i] == 0) {
                while (zeroIndex < i && nums[zeroIndex] == 0) {
                    zeroIndex++;
                }
                int temp = nums[zeroIndex];
                nums[zeroIndex] = nums[i];
                nums[i] = temp;
                if (nums[i] == 2) {
                    i--;
                }
            } else if (nums[i] == 2) {
                while (i < twoIndex && nums[twoIndex] == 2) {
                    twoIndex--;
                }
                int temp = nums[twoIndex];
                nums[twoIndex] = nums[i];
                nums[i] = temp;
                if (nums[i] == 0) {
                    i--;
                }
            }
        }
        System.out.println(Arrays.toString(nums));
    }

    // 1------------------------------------------------------------
    // bubble sort
    public static void sortColors(int[] nums) {
        for (int i = nums.length - 1; i >= 0; i--) {
            for (int j = 1; j <= i; j++) {
                if (nums[j - 1] > nums[j]) {
                    int tmp = nums[j];
                    nums[j] = nums[j - 1];
                    nums[j - 1] = tmp;
                }
            }
        }
    }

}
