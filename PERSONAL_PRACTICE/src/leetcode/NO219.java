package leetcode;

import java.util.HashSet;
import java.util.Set;

//ok
public class NO219 {

    public static void main(String[] args) {
        // System.out.println(containsNearbyDuplicate(new int[] { 13, 23, 1, 2,
        // 3 }, 5));
        System.out.println(containsNearbyDuplicate(new int[] { 1, 2, 3, 1, 2, 3 }, 2));
    }

    public static boolean containsNearbyDuplicate(int[] nums, int k) {
        // string
        // fill
        int len = nums.length;
        // ***[array] string can't not serve as array
        Set<Integer> set = new HashSet<Integer>();
        for (int i = 0; i < len; i++) {
            if (i >= k + 1) {
                set.remove(nums[i - (k + 1)]);
            }
            if (set.contains(nums[i])) {
                return true;
            } else {
                // ***[collection] int can be added to collection directly
                set.add(nums[i]);
            }
        }
        return false;

    }
}
