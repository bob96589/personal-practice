package algorithm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

//preOrder, postOrder, 可用來做Topological Sort
//array, graph, matrix, binary tree, graph
public class DFS_RECURSIVE {

    // 5-----------------------------------------------------------------------------------------------------
    // ***[dfs]走訪陣列
    private static void dfs2(int[] nums, int index, Set<List<Integer>> result, List<Integer> temp) {
        if (temp.size() >= 2) {
            // ***[list]複製list
            result.add(new ArrayList(temp));
        }
        // ***[for]小心i, index會混淆
        for (int i = index; i < nums.length; i++) {
            if (temp.size() == 0 || temp.get(temp.size() - 1) <= nums[i]) {
                // ***[dfs]preOrder做的事，在postOrder要還原
                temp.add(nums[i]);
                dfs2(nums, i + 1, result, temp);
                temp.remove(temp.size() - 1);
            }
        }

    }

    // 4-----------------------------------------------------------------------------------------------------
    // graph
    private static boolean dfs(List<List<Integer>> adjList, int numCourse, boolean[] visited) {
        if (visited[numCourse]) {
            return false;
        }
        visited[numCourse] = true;
        List<Integer> outList = adjList.get(numCourse);
        for (Iterator<Integer> iterator = outList.iterator(); iterator.hasNext();) {
            Integer out = iterator.next();
            // ***[dfs]檢查有沒有cycle
            if (!dfs(adjList, out, visited)) {
                return false;
            }
            // ***[dfs]檢查完，就從原本大樹中移除
            iterator.remove();
        }
        visited[numCourse] = false;
        return true;
    }

    // 3-----------------------------------------------------------------------------------------------------
    // matrix
    // ***[dfs] matrix dfs, better
    public static void dfs_matrix(char[][] grid, int iIndex, int jIndex) {
        int iLen = grid.length;
        int jLen = grid[0].length;
        grid[iIndex][jIndex] = '0';
        // up
        if (iIndex - 1 >= 0 && grid[iIndex - 1][jIndex] == '1') {
            dfs_matrix(grid, iIndex - 1, jIndex);
        }
        // left
        if (jIndex - 1 >= 0 && grid[iIndex][jIndex - 1] == '1') {
            dfs_matrix(grid, iIndex, jIndex - 1);
        }
        // down
        if (iIndex + 1 < iLen && grid[iIndex + 1][jIndex] == '1') {
            dfs_matrix(grid, iIndex + 1, jIndex);
        }
        // right
        if (jIndex + 1 < jLen && grid[iIndex][jIndex + 1] == '1') {
            dfs_matrix(grid, iIndex, jIndex + 1);
        }
    }

    // 2-----------------------------------------------------------------------------------------------------
    // binary tree
    // ***[dfs] binary tree dfs, better
    private void dfs2(TreeNode root, List<String> memo, String str) {
        if (root.left == null && root.right == null) {
            memo.add(str + root.val);
        }
        if (root.left != null) {
            dfs2(root.left, memo, str + root.val + "->");
        }
        if (root.right != null) {
            dfs2(root.right, memo, str + root.val + "->");
        }
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }

        @Override
        public String toString() {
            return Integer.toString(val);
        }
    }

    // 1-----------------------------------------------------------------------------------------------------
    // graph
    // recursive dfs, 無法檢查出有無cycle
    public static void dfs(ArrayList<ArrayList<Integer>> adjLists, boolean[] visited, int startItem, List<Integer> preOrder, List<Integer> postOrder) {
        visited[startItem] = true;
        preOrder.add(startItem);
        System.out.println("item visited (inOrder): " + startItem);
        ArrayList<Integer> list = adjLists.get(startItem);
        for (int item : list) {
            if (!visited[item]) {
                dfs(adjLists, visited, item, preOrder, postOrder);
            }
        }
        postOrder.add(startItem);
        System.out.println("item finished (postOrder): " + startItem);
    }

    // Testing our implementation
    public static void main(String[] args) {
        ArrayList<ArrayList<Integer>> adjLists = new ArrayList<ArrayList<Integer>>();
        final int n = 7;
        for (int i = 0; i < n; i++) {
            adjLists.add(new ArrayList<Integer>());
        }
        adjLists.get(0).add(1);
        adjLists.get(0).add(2);
        adjLists.get(0).add(3);
        adjLists.get(1).add(5);
        adjLists.get(1).add(6);
        adjLists.get(2).add(4);
        adjLists.get(3).add(2);
        adjLists.get(3).add(4);
        adjLists.get(4).add(1);
        adjLists.get(6).add(4);
        boolean[] visited = new boolean[adjLists.size()];

        List<Integer> preOrder = new ArrayList<Integer>();
        List<Integer> postOrder = new ArrayList<Integer>();
        dfs(adjLists, visited, 0, preOrder, postOrder);
        System.out.println("Pre Order Traversal: " + preOrder.toString());
        System.out.println("Post Order Traversal: " + postOrder.toString());

    }

}
