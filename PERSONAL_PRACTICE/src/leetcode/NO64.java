package leetcode;

import java.util.Arrays;

public class NO64 {

    public static void main(String[] args) {
        // ***[array]initialize two dimensional array
        int m = 8;
        int n = 8;
        int[][] memo = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                memo[i][j] = -1;
            }
        }
        // ***[array] can not directly print int array, use Arrays.toString
        // ***[array] can not print two dimensional array directly
        int a[] = new int[] { 1, 2 };
        System.out.println(Arrays.toString(a));
        Integer b[] = new Integer[] { 1, 2 };
        System.out.println(Arrays.toString(b));
        // ***[array] declare two dimensional array
        int[][] grid = { { 1, 2 }, { 1, 1 } };
        // System.out.println(minPathSum(grid));
        System.out.println(minPathSum2(grid));
    }

    // ***[dynamic programming] memoization, top down
    public static int minPathSum2(int[][] grid) {
        int m = grid.length;
        int n = grid[0].length;
        Integer[][] memo = new Integer[m][n];
        memo[0][0] = grid[0][0];
        return getMin(m - 1, n - 1, grid, memo);
    }

    public static int getMin(int i, int j, int[][] grid, Integer[][] memo) {
        if (i == 0 && j == 0) {
            return grid[0][0];
        }
        // up
        int up;
        if (i == 0) {
            up = Integer.MAX_VALUE;
        } else if (memo[i - 1][j] != null) {
            up = memo[i - 1][j];
        } else {
            up = memo[i - 1][j] = getMin(i - 1, j, grid, memo);
        }
        // left
        int left;
        if (j == 0) {
            left = Integer.MAX_VALUE;
        } else if (memo[i][j - 1] != null) {
            left = memo[i][j - 1];
        } else {
            left = memo[i][j - 1] = getMin(i, j - 1, grid, memo);
        }
        return Math.min(up, left) + grid[i][j];
    }

    // ***[dynamic programming] bottom up, easier
    public static int minPathSum(int[][] grid) {

        int m = grid.length;
        int n = grid[0].length;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int up;
                int left;
                if (i != 0 && j != 0) {
                    up = grid[i - 1][j];
                    left = grid[i][j - 1];
                    grid[i][j] += Math.min(up, left);
                } else if (i != 0) {
                    up = grid[i - 1][j];
                    grid[i][j] += up;
                } else if (j != 0) {
                    left = grid[i][j - 1];
                    grid[i][j] += left;
                }
            }
        }

        return grid[m - 1][n - 1];
    }

}
