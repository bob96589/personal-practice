package leetcode;

import java.util.HashMap;
import java.util.Map;

public class NO447 {

    public static void main(String[] args) {
        System.out.println(numberOfBoomerangs(new int[][] { { 0, 0 }, { 1, 0 }, { 2, 0 } }));

        // [[0,0],[1,0],[2,0]]: 2

    }

    // 1----------------------------------------------------------------------
    public static int numberOfBoomerangs(int[][] points) {
        int iLen = points.length;

        // @@@[for]若迴圈能合併會更好
        String[][] distance = new String[iLen][iLen];
        for (int i = 0; i < iLen; i++) {
            for (int j = i + 1; j < iLen; j++) {
                String d = getDistance(points, i, j);
                distance[i][j] = d;
                distance[j][i] = d;
            }
        }

        int result = 0;
        for (int i = 0; i < iLen; i++) {
            Map<String, Integer> map = new HashMap<String, Integer>();
            for (int j = 0; j < iLen; j++) {
                if (i == j) {
                    continue;
                }
                String d = distance[i][j];
                if (!map.containsKey(d)) {
                    map.put(d, 1);
                } else {
                    int cnt = map.get(d).intValue();
                    map.put(d, cnt + 1);
                }
            }
            for (String key : map.keySet()) {
                int value = map.get(key).intValue();
                if (value >= 2) {
                    result += (value * (value - 1));
                }
            }

        }

        return result;

    }

    // @@@[int]C幾取幾
    // private static int cFunction(int a, int b) {
    // if (cFuntionMemo[a][b] != null) {
    // return cFuntionMemo[a][b];
    // }
    // if (a == b) {
    // return 1;
    // }
    // if (b == 1) {
    // return a;
    // }
    // return cFunction(a - 1, b) + cFunction(a - 1, b - 1);
    // }

    private static String getDistance(int[][] points, int index1, int index2) {
        int x1 = points[index1][0];
        int y1 = points[index1][1];
        int x2 = points[index2][0];
        int y2 = points[index2][1];
        int x = x1 - x2;
        int y = y1 - y2;
        // @@@[int] 平方根，次方
        // double l = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
        // @@@[common]變數宣告越少越快
        return String.valueOf((int) (Math.pow(x, 2) + Math.pow(y, 2)));
    }

}
