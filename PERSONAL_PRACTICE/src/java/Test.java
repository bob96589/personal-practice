package java;

public class Test {
    public static void main(String[] args) {
        HelloThread t1 = new HelloThread();
        HelloThread t2 = new HelloThread();
        t1.setName("T1");
        t1.setName("T2");
        t1.start();
        t2.start();
        // 取得目前執行緒數量
        System.out.println(Thread.activeCount());
    }
}

class HelloThread extends Thread {
    public void run() {
        for (int i = 1; i < 100; i++) {
            // 取得目前執行緒名稱
            String tName = Thread.currentThread().getName();
            System.out.println(tName + ":" + i);
        }
    }
}
