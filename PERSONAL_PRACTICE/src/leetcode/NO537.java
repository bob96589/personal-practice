package leetcode;

public class NO537 {

    public static void main(String[] args) {
        System.out.println(complexNumberMultiply("1+-1i", "0+0i"));
    }

    public static String complexNumberMultiply(String a, String b) {

        Integer a1 = Integer.parseInt(a.substring(0, a.indexOf("+")));
        Integer a2 = Integer.parseInt(a.substring(a.indexOf("+") + 1, a.indexOf("i")));

        Integer b1 = Integer.parseInt(b.substring(0, b.indexOf("+")));
        Integer b2 = Integer.parseInt(b.substring(b.indexOf("+") + 1, b.indexOf("i")));

        int a3 = (a1 * b1) + (a2 * b2 * (-1));
        int b3 = a1 * b2 + a2 * b1;

        return a3 + "+" + b3 + "i";
    }

}
