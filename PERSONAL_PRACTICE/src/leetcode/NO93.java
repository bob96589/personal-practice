package leetcode;

import java.util.ArrayList;
import java.util.List;

public class NO93 {

    public static void main(String[] args) {
        restoreIpAddresses("25525511135");
    }

    public static List<String> restoreIpAddresses(String s) {
        List<String> resultList = new ArrayList<String>();
        backTracking(s, 0, 0, new ArrayList<Integer>(), resultList);
        return resultList;
    }

    // ***[backtracking]
    public static void backTracking(String s, int startPos, int endPos, List<Integer> temp, List<String> resultList) {
        if (temp.size() == 4) {
            if (endPos == s.length()) {
                StringBuffer sb = new StringBuffer();
                boolean isFirst = true;
                for (Integer i : temp) {
                    if (isFirst) {
                        isFirst = false;
                    } else {
                        sb.append(".");
                    }
                    sb.append(i);
                }
                resultList.add(sb.toString());
            } else {
                return;
            }
        }
        for (int i = 1; i <= 3; i++) {// 1,2,3
            if (endPos + i > s.length()) {
                break;
            }
            int num = Integer.parseInt(s.substring(endPos, endPos + i));
            if (num > 255 || Integer.toString(num).length() != i) {
                return;
            } else {
                temp.add(num);
                backTracking(s, endPos, endPos + i, temp, resultList);
                temp.remove(temp.size() - 1);
            }
        }
    }

}
