package leetcode;

//ok
public class NO520 {

    public static void main(String[] args) {
        System.out.println(detectCapitalUse("USA"));
        System.out.println(detectCapitalUse("leetcode"));
        System.out.println(detectCapitalUse("Google"));
        System.out.println(detectCapitalUse("FlaG"));

        System.out.println(detectCapitalUse2("USA"));
        System.out.println(detectCapitalUse2("leetcode"));
        System.out.println(detectCapitalUse2("Google"));
        System.out.println(detectCapitalUse2("FlaG"));
        // \*\*\*[string]convert char into ascii
        // System.out.println((int) 'a');
        // System.out.println((int) 'z');
        // System.out.println((int) 'A');
        // System.out.println((int) 'Z');

    }

    public static boolean detectCapitalUse(String word) {
        // all capital
        if (checkLetter(word, true)) {
            return true;
        }
        // all not capital
        if (checkLetter(word, false)) {
            return true;
        }
        // first capital, other not capital
        if (checkLetter(word.substring(0, 1), true) && checkLetter(word.substring(1), false)) {
            return true;
        }
        return false;
    }

    // all capital, all not capital
    private static boolean checkLetter(String word, boolean capital) {
        // 97 122
        // 65 90
        int startCode;
        int endCode;
        if (capital) {
            startCode = 65;
            endCode = 90;
        } else {
            startCode = 97;
            endCode = 122;
        }
        int wordLength = word.length() - 1;
        for (int i = 0; i <= wordLength; i++) {
            int code = (int) word.charAt(i);
            if (startCode <= code && code <= endCode) {
                continue;
            } else {
                return false;
            }
        }
        return true;
    }

    public static boolean detectCapitalUse2(String word) {
        // ***[string]string regex
        return word.matches("[A-Z]+|[a-z]+|[A-Z][a-z]+");
    }

}
