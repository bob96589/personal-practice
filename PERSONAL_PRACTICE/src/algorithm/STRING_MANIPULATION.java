package algorithm;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class STRING_MANIPULATION {

    public static void main(String[] args) {
        removeDuplicate4();
    }

    // -----------------------------------------------------------------------
    // remove duplicate string
    // -----------------------------------------------------------------------

    public static String removeDuplicate(String s) {
        String string = "aabbccdefatafaz";

        char[] chars = string.toCharArray();
        Set<Character> charSet = new LinkedHashSet<Character>();
        for (char c : chars) {
            charSet.add(c);
        }

        StringBuilder sb = new StringBuilder();
        for (Character character : charSet) {
            sb.append(character);
        }
        return sb.toString();
    }

    // --------------------------------------------------

    public String removeDuplicate2(String input) {
        String result = "";
        for (int i = 0; i < input.length(); i++) {
            if (!result.contains(String.valueOf(input.charAt(i)))) {
                result += String.valueOf(input.charAt(i));
            }
        }
        return result;
    }

    // --------------------------------------------------

    public static String removeDuplicate3() {
        // remove duplicate string in string array
        String[] strAry = new String[] { "aa", "bb", "cc", "aa", "bb" };
        // string[] to set
        Set<String> set = new HashSet<String>(Arrays.asList(strAry));
        // set to string[]
        strAry = set.toArray(new String[0]);
        // print string[]
        System.out.println(Arrays.toString(strAry));
        return "";
    }

    // --------------------------------------------------

    public static String removeDuplicate4() {
        // remove duplicate neightbor
        // compare with previous one
        String str = "112244454433223311";
        // @@@[string] 若要刪除字串的某幾個字元，就要new stringbuffer
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            // @@@[string]compare prev, item 0 don't do the comparison
            if (i == 0) {
                sb.append(str.charAt(i));
            } else {
                if (str.charAt(i) != str.charAt(i - 1)) {
                    sb.append(str.charAt(i));
                }
            }
        }
        System.out.println(sb.toString());
        return "";
    }

    // -----------------------------------------------------------------------
    // reverse string
    // -----------------------------------------------------------------------

    public static String reverseString(String s) {
        int i = 0, j = s.length() - 1;
        char[] str = s.toCharArray();
        while (i < j) {
            char temp = str[i];
            str[i] = str[j];
            str[j] = temp;
            i++;
            j--;
        }
        String ss = new String(str);
        return ss;
    }

    // --------------------------------------------------

    public String reverseString2(String s) {
        String reverse = new StringBuffer(s).reverse().toString();
        return reverse;
    }

}
