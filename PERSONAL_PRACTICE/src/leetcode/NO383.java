package leetcode;

import java.util.HashMap;
import java.util.Map;

public class NO383 {

    public static void main(String[] args) {
        System.out.println(canConstruct2("aa", "ab"));

    }

    public static boolean canConstruct2(String ransomNote, String magazine) {

        char[] magazineChar = magazine.toCharArray();
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        for (int i = 0; i < magazineChar.length; i++) {
            char c = magazineChar[i];
            if (!map.containsKey(c)) {
                map.put(c, 1);
            } else {
                // @@@[map] integer in map calculate
                Integer num = map.get(c);
                map.put(c, num.intValue() + 1);
            }
        }

        char[] ransomNoteChar = ransomNote.toCharArray();
        for (char c : ransomNoteChar) {
            if (map.containsKey(c)) {
                Integer num = map.get(c);
                if (num.equals(new Integer(0))) {
                    return false;
                }
                map.put(c, num.intValue() - 1);
            } else {
                return false;
            }
        }

        return true;
    }

    public static boolean canConstruct(String ransomNote, String magazine) {
        int[] array = new int[26];
        for (int i = 0; i < magazine.length(); i++) {
            array[magazine.charAt(i) - 'a']++;
        }
        for (int i = 0; i < ransomNote.length(); i++) {
            if (array[ransomNote.charAt(i) - 'a'] == 0) {
                return false;
            } else {
                array[ransomNote.charAt(i) - 'a']--;
            }
        }
        return true;
    }

}
