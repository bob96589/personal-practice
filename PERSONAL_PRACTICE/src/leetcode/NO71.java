package leetcode;

import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class NO71 {

    public static void main(String[] args) {
        System.out.println(simplifyPath2("/home/")); // /home
        System.out.println(simplifyPath2("/a/./b/../../c/")); // /c
        System.out.println(simplifyPath2("/../")); // /
        System.out.println(simplifyPath2("/home//foo/")); // /home/foo
        System.out.println(simplifyPath2("/...")); // /...
        System.out.println(simplifyPath2("/..hidden")); // "/..hidden"

        // [, home]
        // [, a, ., b, .., .., c]
        // [, ..]
        // [, home, , foo]
        // [, ...]

    }

    public static String simplifyPath2(String path) {
        Deque<String> stack = new LinkedList<String>();
        // @@@[set] skip element
        // @@@[collections] declare collections
        Set<String> skip = new HashSet<>(Arrays.asList("..", ".", ""));
        for (String dir : path.split("/")) {
            if (dir.equals("..") && !stack.isEmpty()) {
                stack.pop();
            } else if (!skip.contains(dir)) {
                stack.push(dir);
            }
        }
        String res = "";
        for (String dir : stack) {
            res = "/" + dir + res;
        }
        return res.isEmpty() ? "/" : res;
    }

    // 1--------------------------------------------------------------------------
    // wrong answer
    public static String simplifyPath(String path) {
        // @@@[string] string escapse
        String[] strAry = path.split("\\.");
        String lastStr = strAry[strAry.length - 1];
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < lastStr.length(); i++) {
            if (i == 0) {
                sb.append(lastStr.charAt(i));
            } else if (i == lastStr.length() - 1) {
                if (lastStr.charAt(i) == '/') {
                    continue;
                } else {
                    sb.append(lastStr.charAt(i));
                }
            } else {
                if (lastStr.charAt(i) == '/' && lastStr.charAt(i - 1) == '/') {
                    continue;
                } else {
                    sb.append(lastStr.charAt(i));
                }
            }
        }
        return sb.toString();
    }

}
