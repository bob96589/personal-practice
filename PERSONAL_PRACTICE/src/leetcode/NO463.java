package leetcode;

public class NO463 {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

    public static int islandPerimeter(int[][] grid) {
        int iLen = grid.length;
        int jLen = grid[0].length;
        int resultCnt = 0;

        for (int i = 0; i < iLen; i++) {
            int prev = 0;
            for (int j = 0; j < jLen; j++) {
                int current = grid[i][j];
                if (prev != current) {
                    resultCnt++;
                }
                prev = current;
            }
            if (prev == 1) {
                resultCnt++;
            }
        }

        for (int j = 0; j < jLen; j++) {
            int prev = 0;
            for (int i = 0; i < iLen; i++) {
                int current = grid[i][j];
                if (prev != current) {
                    resultCnt++;
                }
                prev = current;
            }
            if (prev == 1) {
                resultCnt++;
            }
        }

        return resultCnt;
    }

}
