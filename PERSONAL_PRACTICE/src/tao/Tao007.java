package tao;

public class Tao007 {
    public static void main(String[] args) {
        int[] nums = new int[] { 7708, 2820, 4032, 7158, 2428, 708, 5191, 3942, 5637, 3381, 9308, 9496, 1162, 4065, 177, 473, 7866, 6200, 4126, 2847, 6405, 5764, 2570, 6483, 7172, 9799, 5458 };
        int max, min;
        max = min = nums[0];
        for (int i = 1; i < nums.length; i++) {
            max = Math.max(max, nums[i]);
            min = Math.min(min, nums[i]);
        }
        System.out.println(max - min);
    }
}
