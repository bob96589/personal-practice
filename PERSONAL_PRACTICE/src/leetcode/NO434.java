package leetcode;

//ok
public class NO434 {

    public static void main(String[] args) {
        System.out.println(countSegments("Hello, my name is John"));
        System.out.println(countSegments(", , , ,        a, eaefa"));
    }

    public static int countSegments(String s) {
        // ***[string] there might be empty string after string split
        String[] strAry = s.split(" ");
        int i = 0;
        for (String str : strAry) {
            // ***[string] isEmpty
            if (!str.trim().isEmpty()) {
                i++;
            }
        }
        return i;
    }
}
