package goanimate;

import java.util.Scanner;

public class Test03 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
    }

    static String isPangram(String[] strings) {
        StringBuffer result = new StringBuffer();
        for (String str : strings) {
            String temp = getCheckVal(str);
            result.append(temp);
        }
        return result.toString();
    }

    public static String getCheckVal(String str) {
        for (char i = 'a'; i <= 'z'; i++) {
            if ((str.indexOf(i) < 0)) {
                return "0";
            }
        }
        return "1";
    }

}
