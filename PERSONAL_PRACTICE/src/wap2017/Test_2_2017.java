package wap2017;

import java.util.Scanner;

public class Test_2_2017 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int cnt = in.nextInt();
        int divisor = in.nextInt();
        int[] nums = new int[cnt];
        for (int i = 0; i < cnt; i++) {
            nums[i] = in.nextInt();
        }
        long start = System.currentTimeMillis();
        int sum = 0;
        if (dfs(nums, -1, sum, divisor)) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
        long end = System.currentTimeMillis();
        System.out.println((end - start) + " ms");
    }

    public static boolean dfs(int[] nums, int index, int sum, int divisor) {
        sum %= divisor;
        if (index >= 0 && sum == 0) {
            return true;
        }
        for (int i = index + 1; i < nums.length; i++) {
            // add
            if (dfs(nums, i, sum + nums[i], divisor)) {
                return true;
            }
        }
        return false;
    }
}
