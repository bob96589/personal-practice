package leetcode;

public class NO58 {

    public static void main(String[] args) {
        System.out.println(lengthOfLastWord(" "));
    }

    public static int lengthOfLastWord(String s) {
        String[] strAry = s.split(" ");
        int lastIndex = strAry.length - 1;
        if (lastIndex < 0) {
            return 0;
        }
        return strAry[lastIndex].length();
    }

}
