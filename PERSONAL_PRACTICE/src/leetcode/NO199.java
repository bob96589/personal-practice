package leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class NO199 {

    public static void main(String[] args) {

    }

    public static List<Integer> rightSideView(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        if (root == null) {
            return result;
        }
        LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
        queue.add(root);
        while (!queue.isEmpty()) {
            result.add(queue.getLast().val);
            int len = queue.size();
            for (int i = 0; i < len; i++) {
                TreeNode treeNode = queue.poll();
                TreeNode childLeft = treeNode.left;
                TreeNode childRight = treeNode.right;
                if (childLeft != null) {
                    queue.add(childLeft);
                }
                if (childRight != null) {
                    queue.add(childRight);
                }
            }
        }
        return result;
    }

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

}
