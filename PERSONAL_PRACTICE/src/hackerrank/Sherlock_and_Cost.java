package hackerrank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

//unacceptable answer
public class Sherlock_and_Cost {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            int k = in.nextInt();
            int[] ary = new int[k];
            for (int j = 0; j < k; j++) {
                ary[j] = in.nextInt();
            }
            subsets(ary);
        }
    }

    public static void subsets(int[] nums) {
        List<Integer> totalScoreList = new ArrayList<Integer>();
        backTracking(nums, -1, 0, new ArrayList<Integer>(), totalScoreList);
        Collections.sort(totalScoreList);
        System.out.println(totalScoreList.get(totalScoreList.size() - 1));
    }

    // ***[backtracking]
    public static void backTracking(int[] nums, int pos, int currScore, List<Integer> currList, List<Integer> totalScoreList) {
        if (currList.size() == nums.length) {
            totalScoreList.add(currScore);
            return;
        }
        int newPos = pos + 1;
        currList.add(nums[newPos]);
        backTracking(nums, newPos, newPos == 0 ? currScore : currScore + getScore(currList.get(currList.size() - 1), currList.get(currList.size() - 2)), currList, totalScoreList);
        currList.remove(currList.size() - 1);
        if (nums[newPos] != 1) {
            currList.add(1);
            backTracking(nums, newPos, newPos == 0 ? currScore : currScore + getScore(currList.get(currList.size() - 1), currList.get(currList.size() - 2)), currList, totalScoreList);
            currList.remove(currList.size() - 1);
        }
    }

    public static int getScore(int a, int b) {
        return a > b ? a - b : b - a;
    }

}
