package wap2017;

public class Test_1_2017 {
    public static void main(String[] args) {
        // Scanner in = new Scanner(System.in);
        // int n = in.nextInt();
        // int k = in.nextInt();
        int n = 4;
        int k = 1;
        int pos = 0;
        boolean[] used = new boolean[n + 1];
        int inversionCnt = 0;
        System.out.println(backTracking(n, k, pos, used, inversionCnt));
    }

    public static int backTracking(int n, int k, int pos, boolean[] used, int inversionCnt) {
        if (k < inversionCnt) {// ���ŦX������
            return 0;

        } else if (pos == n && k == inversionCnt) {// �ŦX������
            return 1;
        }
        int result = 0;
        for (int value = 1; value <= n; value++) {// i: value�B�C�@�ӭȳ�������
            if (used[value]) {
                continue;
            }
            int tempInversionCnt = 0;
            for (int j = 1; j < value; j++) {
                if (!used[j]) {
                    tempInversionCnt++;
                }
            }
            used[value] = true;
            result += backTracking(n, k, pos + 1, used, inversionCnt + tempInversionCnt);
            used[value] = false;
        }
        return result;
    }
}
