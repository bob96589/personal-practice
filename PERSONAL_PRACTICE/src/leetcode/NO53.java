package leetcode;

import java.util.Arrays;

//TODO https://discuss.leetcode.com/topic/6413/dp-solution-some-thoughts/2
public class NO53 {

    public static void main(String[] args) {
        int[] nums = new int[] { -2, 1, -3, 4, -1, 2, 1, -5, 4 };// 4, -1, 2, 1
        // System.out.println(maxSubArray(nums));
        System.out.println(maxSubArray2(nums));
        System.out.println(maxSubArray3(nums));
    }

    // ***[dynamic programming] Line 14: java.lang.StackOverflowError,
    // ***[dynamic programming] memoization, top down, 用top down要小心，太難了
    public static int maxSubArray2(int[] nums) {
        // ***[dynamic programming] 答案不是最後的解，是中間的解：傳入陣列參數，取各個recursion的值
        int[] memo = new int[nums.length];
        getMaxSum(nums, nums.length - 1, memo);
        Arrays.sort(memo);
        return memo[nums.length - 1];
    }

    public static int getMaxSum(int[] nums, int endIndex, int[] memo) {
        if (endIndex == 0) {
            memo[endIndex] = nums[endIndex];
            return nums[endIndex];
        }
        int max;
        int lastSum = getMaxSum(nums, endIndex - 1, memo);
        if (lastSum < 0) {
            max = nums[endIndex];
        } else {
            max = nums[endIndex] + lastSum;
        }
        return memo[endIndex] = max;
    }

    // ***[dynamic programming] bottom up, 拿中間的解較容易
    public static int maxSubArray3(int[] nums) {
        int len = nums.length;
        int memo[] = new int[len];
        memo[0] = nums[0];
        int max = memo[0];
        for (int i = 1; i < len; i++) {
            if (memo[i - 1] < 0) {
                memo[i] = nums[i];
            } else {
                memo[i] = memo[i - 1] + nums[i];
            }
            max = Math.max(max, memo[i]);
        }
        return max;
    }

    // ***[dynamic programming] bad solution, 不要一開始就想要填表格
    public static int maxSubArray(int[] nums) {
        int len = nums.length;
        int[][] memo = new int[len][len];
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < len; i++) {
            for (int j = i; j >= 0; j--) {
                if (i == j) {
                    memo[i][j] = nums[i];
                } else {
                    int down = memo[i][j + 1];
                    int left = memo[i - 1][j];
                    int leftDown;
                    if (j + 1 > i - 1) {
                        leftDown = 0;
                    } else {
                        leftDown = memo[i - 1][j + 1];
                    }
                    memo[i][j] = down + left - leftDown;
                }
                max = memo[i][j] > max ? memo[i][j] : max;
            }
        }
        return max;
    }

    // ***[array]print Two Dimensional Ary
    public static void printTwoDimensionalAry(int[][] memo) {
        for (int[] ary : memo) {
            System.out.println(Arrays.toString(ary));
        }
    }

}
