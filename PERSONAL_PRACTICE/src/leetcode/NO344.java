package leetcode;

//TODO
public class NO344 {

    public static void main(String[] args) {
        System.out.println(reverseString("hello"));
    }

    public static String reverseString(String s) {
        char[] charAry = s.toCharArray();
        int length = charAry.length;

        for (int i = 0; i < length / 2; i++) {
            char temp = charAry[i];
            charAry[i] = charAry[length - i - 1];
            charAry[length - i - 1] = temp;
        }
        return String.valueOf(charAry);
    }

    public static String reverseString2(String s) {
        StringBuilder sb = new StringBuilder(s);
        return sb.reverse().toString();
    }

}
