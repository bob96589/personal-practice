package algorithm;

import java.util.Arrays;

public class SORT {

    public static void main(String[] args) {
        bubbleSort(new int[] { 5, 3, 7, 2, 6, 1, 0, 8, 9, 4 });
        selectionSort(new int[] { 5, 3, 7, 2, 6, 1, 0, 8, 9, 4 });
        insertionSort(new int[] { 5, 3, 7, 2, 6, 1, 0, 8, 9, 4 });

    }

    // bubbleSort---------------------------------------------------------------------
    public static void bubbleSort(int[] nums) {
        for (int i = nums.length - 1; i >= 0; i--) {
            for (int j = 1; j <= i; j++) {
                if (nums[j - 1] > nums[j]) {
                    int tmp = nums[j];
                    nums[j] = nums[j - 1];
                    nums[j - 1] = tmp;
                }
            }
        }
        System.out.println(Arrays.toString(nums));
    }

    // selectionSort---------------------------------------------------------------------
    public static void selectionSort(int[] nums) {
        int len = nums.length;
        for (int i = 0; i < len; i++) {
            int minIndex = i;
            for (int j = i + 1; j < len; j++) {
                if (nums[minIndex] > nums[j]) {
                    minIndex = j;
                }
            }
            int tmp = nums[i];
            nums[i] = nums[minIndex];
            nums[minIndex] = tmp;
        }
        System.out.println(Arrays.toString(nums));
    }

    // insertionSort---------------------------------------------------------------------
    public static void insertionSort(int[] nums) {
        int len = nums.length;
        for (int i = 1; i < len; i++) {
            int j = i;
            while (j > 0 && nums[j - 1] > nums[j]) {
                int tmp = nums[j];
                nums[j] = nums[j - 1];
                nums[j - 1] = tmp;
                j--;
            }
        }
        System.out.println(Arrays.toString(nums));
    }

    // mergeSort---------------------------------------------------------------------
    public static void mergeSort(Comparable[] a) {
        Comparable[] tmp = new Comparable[a.length];
        mergeSort(a, tmp, 0, a.length - 1);
    }

    private static void mergeSort(Comparable[] a, Comparable[] tmp, int left, int right) {
        if (left < right) {
            int center = (left + right) / 2;
            mergeSort(a, tmp, left, center);
            mergeSort(a, tmp, center + 1, right);
            merge(a, tmp, left, center + 1, right);
        }
    }

    private static void merge(Comparable[] a, Comparable[] tmp, int left, int right, int rightEnd) {
        int leftEnd = right - 1;
        int k = left;
        int num = rightEnd - left + 1;

        while (left <= leftEnd && right <= rightEnd)
            if (a[left].compareTo(a[right]) <= 0)
                tmp[k++] = a[left++];
            else
                tmp[k++] = a[right++];

        while (left <= leftEnd) // Copy rest of first half
            tmp[k++] = a[left++];

        while (right <= rightEnd) // Copy rest of right half
            tmp[k++] = a[right++];

        // Copy tmp back
        for (int i = 0; i < num; i++, rightEnd--)
            a[rightEnd] = tmp[rightEnd];
    }

}
