package leetcode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

//***[topological sort]測試有無cycle 
public class NO207_CYCLE {

    public static void main(String[] args) {
        System.out.println(canFinish4(2, new int[][] { { 0, 1 }, { 1, 0 } }));

    }

    // 4---------------------------------------------------------------------------------------------------
    // BFS
    // beat 60%
    public static boolean canFinish4(int numCourses, int[][] prerequisites) {
        // 1: create inCnt, outArray
        List<Integer> inCnt = new ArrayList<Integer>(numCourses);
        List<List<Integer>> outLists = new ArrayList<List<Integer>>(numCourses);
        for (int i = 0; i < numCourses; i++) {
            inCnt.add(0);
            outLists.add(new ArrayList<Integer>());
        }
        for (int i = 0; i < prerequisites.length; i++) {
            int zero = prerequisites[i][0];
            int one = prerequisites[i][1];
            // ***[integer] add, integer++
            inCnt.set(zero, inCnt.get(zero).intValue() + 1);
            outLists.get(one).add(zero);
        }

        // 2: find and remove item without in
        Queue<Integer> queue = new LinkedList<Integer>();
        for (int i = 0; i < numCourses; i++) {
            if (inCnt.get(i).equals(0)) {
                queue.add(i);
            }
        }
        while (!queue.isEmpty()) {
            Integer current = queue.remove();

            for (Integer out : outLists.get(current)) {// 有向外相鄰,的每一個
                inCnt.set(out, inCnt.get(out).intValue() - 1);
                if (inCnt.get(out).equals(0)) {// 無向內相鄰
                    queue.add(out);
                }
            }
        }
        // 3: search the existing inList or outList, to see if there is a cycle
        for (int i = 0; i < numCourses; i++) {
            if (!inCnt.get(i).equals(0)) {
                return false;
            }
        }
        return true;
    }

    // 3---------------------------------------------------------------------------------------------------
    // BFS
    // beat 34%
    public static boolean canFinish3(int numCourses, int[][] prerequisites) {
        // 1: create inCnt, outArray
        // ***[dfs] in只是要看數量，out是要看到哪個點
        int[] inCnt = new int[numCourses];
        // ***[matrix]這個matrix太大了
        int[][] outArray = new int[numCourses][numCourses];
        for (int i = 0; i < prerequisites.length; i++) {
            int zero = prerequisites[i][0];
            int one = prerequisites[i][1];
            inCnt[zero]++;
            outArray[one][zero] = 1;
        }

        // 2: find and remove item without in
        Queue<Integer> queue = new LinkedList<Integer>();
        for (int i = 0; i < numCourses; i++) {
            if (inCnt[i] == 0) {
                queue.add(i);
            }
        }
        while (!queue.isEmpty()) {
            Integer current = queue.remove();
            for (int i = 0; i < numCourses; i++) {
                if (outArray[current][i] == 1) {// 有向外相鄰
                    inCnt[i]--;
                    if (inCnt[i] == 0) {// 無向內相鄰
                        queue.add(i);
                    }
                }
            }
        }
        // 3: search the existing inList or outList, to see if there is a cycle
        for (int i = 0; i < numCourses; i++) {
            if (inCnt[i] != 0) {
                return false;
            }
        }
        return true;
    }

    // 2---------------------------------------------------------------------------------------------------
    // BFS
    // beat 56%
    public static boolean canFinish2(int numCourses, int[][] prerequisites) {
        // 1: create, fill in out adjacency list
        // ***[dfs] in只是要看數量，out是要看到哪個點
        List<List<Integer>> inLists = new ArrayList<List<Integer>>();
        List<List<Integer>> outLists = new ArrayList<List<Integer>>();
        for (int i = 0; i < numCourses; i++) {
            inLists.add(new ArrayList<Integer>());
            outLists.add(new ArrayList<Integer>());
        }
        for (int i = 0; i < prerequisites.length; i++) {
            int zero = prerequisites[i][0];
            int one = prerequisites[i][1];
            inLists.get(zero).add(one);
            outLists.get(one).add(zero);
        }
        // 2: find and remove item without in list
        List<Integer> nodeWithEmptyInList = new ArrayList<Integer>();
        List<Integer> nextNodeWithEmptyInList = new ArrayList<Integer>();
        // find node With Empty InList
        for (int i = 0; i < numCourses; i++) {
            List<Integer> inList = inLists.get(i);
            if (inList.isEmpty()) {
                nodeWithEmptyInList.add(i);
            }
        }
        // ***[list] next level iterator
        while (!nodeWithEmptyInList.isEmpty()) {
            for (Integer integer : nodeWithEmptyInList) {
                List<Integer> outList = outLists.get(integer);
                for (Iterator<Integer> it = outList.iterator(); it.hasNext();) {
                    Integer out = it.next();
                    inLists.get(out).remove(integer);
                    it.remove();
                    if (inLists.get(out).size() == 0) {
                        nextNodeWithEmptyInList.add(out);
                    }
                }
            }
            nodeWithEmptyInList = nextNodeWithEmptyInList;
            nextNodeWithEmptyInList = new ArrayList<Integer>();
        }
        // 3: search the existing inList or outList, to see if there is a cycle
        for (List<Integer> inList : inLists) {
            if (!inList.isEmpty()) {
                return false;
            }
        }
        return true;

    }

    // 1--------------------------------------------------------------------------------------------------------------
    // TOPOLOGICAL_SORT_DFS, beat 84%
    public static boolean canFinish(int numCourses, int[][] prerequisites) {
        List<List<Integer>> adjList = new ArrayList<List<Integer>>();
        for (int i = 0; i < numCourses; i++) {
            adjList.add(new ArrayList<Integer>());
        }
        for (int[] prerequisite : prerequisites) {
            int first = prerequisite[0];
            int second = prerequisite[1];
            adjList.get(second).add(first);
        }

        boolean[] visited = new boolean[numCourses];
        for (int i = 0; i < numCourses; i++) {
            if (dfs(adjList, i, visited) == false) {
                return false;
            }
        }
        return true;
    }

    private static boolean dfs(List<List<Integer>> adjList, int numCourse, boolean[] visited) {
        if (visited[numCourse]) {
            return false;
        }
        visited[numCourse] = true;
        List<Integer> outList = adjList.get(numCourse);
        for (Iterator<Integer> iterator = outList.iterator(); iterator.hasNext();) {
            Integer out = iterator.next();
            // ***[dfs]檢查有沒有cycle
            if (!dfs(adjList, out, visited)) {
                return false;
            }
            // ***[dfs]檢查完，就從原本大樹中移除
            iterator.remove();
        }
        visited[numCourse] = false;
        return true;
    }
}
