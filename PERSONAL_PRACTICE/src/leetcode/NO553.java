package leetcode;

public class NO553 {

    public static void main(String[] args) {
        // System.out.println(optimalDivision(new int[] { 1000, 100, 10, 2 }));
        System.out.println(optimalDivision2(new int[] { 1000, 100, 10, 2 }));
        // Submission Result: Wrong Answer More Details
        // Input:
        // [6,2,3,4,5]
        // Output:
        // "6/(2/3/(4/5))"
        // Expected:
        // "6/(2/3/4/5)"
    }

    public static String optimalDivision2(int[] nums) {

        StringBuffer sb = new StringBuffer();

        int len = nums.length;
        int cnt = 0;
        for (int i = 0; i < len; i++) {
            if (i != 0) {
                sb.append("/");
            }
            if (i == 1 && i != len - 1) {
                sb.append("(");
                cnt++;
            }
            sb.append(nums[i]);
        }
        for (int i = 0; i < cnt; i++) {
            sb.append(")");
        }
        return sb.toString();

    }

}
