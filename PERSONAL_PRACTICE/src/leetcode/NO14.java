package leetcode;

public class NO14 {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

    public static String longestCommonPrefix(String[] strs) {
        // first length
        int strLen = strs.length;
        if (strLen == 0) {
            return "";
        }
        int firstLen = strs[0].length();
        String firstStr = strs[0];
        int endIndex = 0;
        outer: for (int i = 0; i < firstLen; i++) {// start from which character
            for (int j = 1; j < strLen; j++) {
                if (strs[j].length() <= i || strs[j].charAt(i) != firstStr.charAt(i)) {
                    endIndex = i;
                    break outer;
                }
            }
            endIndex = i + 1;
        }
        return firstStr.substring(0, endIndex);
    }
}
