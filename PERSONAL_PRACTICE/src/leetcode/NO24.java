package leetcode;

//TODO
public class NO24 {

    public static void main(String[] args) {

    }

    public static ListNode swapPairs(ListNode head) {

        ListNode first = head;
        if (first == null) {
            return null;
        }
        ListNode second = head.next;
        if (second == null) {
            return first;
        }
        ListNode third = head.next.next;

        second.next = first;
        first.next = swapPairs(third);

        return second;

    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

}
