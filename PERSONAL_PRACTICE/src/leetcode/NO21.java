package leetcode;

//ok
public class NO21 {

    public static void main(String[] args) {

    }

    // ***[list] merge two list
    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {

        if (l1 == null) {
            return l2;
        } else if (l2 == null) {
            return l1;
        }

        if (l1.val < l2.val) {
            l1.next = mergeTwoLists(l1.next, l2);
            return l1;
        } else {
            l2.next = mergeTwoLists(l1, l2.next);
            return l2;
        }

    }

    public static ListNode mergeTwoLists2(ListNode l1, ListNode l2) {

        ListNode head = null;
        ListNode current = null;
        while (true) {

            if (l1 == null) {
                if (head == null) {
                    head = l2;
                    current = l2;
                } else {
                    current.next = l2;
                    current = current.next;
                }
                break;
            } else if (l2 == null) {
                if (head == null) {
                    head = l1;
                    current = l1;
                } else {
                    current.next = l1;
                    current = current.next;
                }
                break;
            }

            if (l1.val < l2.val) {
                if (head == null) {
                    head = l1;
                    current = l1;
                } else {
                    current.next = l1;
                    current = current.next;
                }
                l1 = l1.next;
            } else {
                if (head == null) {
                    head = l2;
                    current = l2;
                } else {
                    current.next = l2;
                    current = current.next;
                }
                l2 = l2.next;
            }
        }
        return head;
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

}
