package leetcode;

import java.util.Stack;

public class NO32 {
    public static void main(String[] args) {
        System.out.println(longestValidParentheses("(()"));// 2
        System.out.println(longestValidParentheses(")()())"));// 4
        System.out.println(longestValidParentheses("(())"));// 4
        System.out.println(longestValidParentheses("()(()"));// 2
        System.out.println(longestValidParentheses("()(())"));// 6
        System.out.println(longestValidParentheses("(()(((()"));// 2

    }

    public static int longestValidParentheses(String s) {

        int len = s.length();
        Stack<Integer> stack = new Stack<Integer>();
        int end = -1;
        int max = 0;

        for (int i = 0; i < len; i++) {
            String str = s.substring(i, i + 1);
            if ("(".equals(str)) {
                stack.push(i);
            } else {
                if (stack.isEmpty()) {
                    end = i;
                } else {
                    stack.pop();
                    if (stack.isEmpty()) {
                        max = Math.max(i - end, max);
                    } else {
                        // ***[stack]Looks at the object at the top of this
                        // stack without removing
                        max = Math.max(i - stack.peek(), max);
                    }
                }
            }

        }
        return max;

    }
}
