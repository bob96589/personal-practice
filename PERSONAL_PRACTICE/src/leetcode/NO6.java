package leetcode;

import java.util.ArrayList;
import java.util.List;

public class NO6 {

    public static void main(String[] args) {
        System.out.println(convert("ABCD", 3));
    }

    public static String convert(String s, int numRows) {
        int len = s.length();
        if (len == 0 || len == 1 || numRows == 1) {
            return s;
        }

        int cntPerUnit = 2 * numRows - 2;
        char[] charAry = s.toCharArray();

        List<List<Character>> list = new ArrayList<List<Character>>();
        for (int i = 0; i < cntPerUnit; i++) {
            list.add(new ArrayList<Character>());
        }
        for (int i = 0; i < len; i++) {
            int indexInList = i % cntPerUnit;
            list.get(indexInList).add(charAry[i]);
        }

        // @@@[list] List<Character> to string
        StringBuffer headSb = new StringBuffer();
        for (char c : list.get(0)) {
            headSb.append(c);
        }
        StringBuffer tailSb = new StringBuffer();
        for (char c : list.get(numRows - 1)) {
            tailSb.append(c);
        }
        int headIndex = 1;
        int tailIndex = cntPerUnit - 1;
        StringBuffer middleSb = new StringBuffer();
        while (headIndex < numRows - 1 && numRows - 1 < tailIndex) {
            List<Character> headlist = list.get(headIndex);
            List<Character> taillist = list.get(tailIndex);
            int headSize = headlist.size();
            int tailSize = taillist.size();
            for (int i = 0; i < headSize; i++) {
                middleSb.append(headlist.get(i));
                if (i < tailSize) {
                    middleSb.append(taillist.get(i));
                }
            }
            headIndex++;
            tailIndex--;
        }
        StringBuffer resultSb = new StringBuffer();
        resultSb.append(headSb);
        resultSb.append(middleSb);
        resultSb.append(tailSb);

        return resultSb.toString();
    }

}
