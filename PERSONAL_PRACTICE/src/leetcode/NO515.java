package leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class NO515 {

    public static void main(String[] args) {
        TreeNode t1 = new TreeNode(1);
        TreeNode t2 = new TreeNode(2);
        TreeNode t3 = new TreeNode(3);
        TreeNode t4 = new TreeNode(4);
        t1.left = t2;
        t1.left.left = t3;
        t1.right = t4;
        System.out.println(largestValues(t1));
    }

    public static List<Integer> largestValues(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        if (root == null) {
            return result;
        }
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.add(root);
        int level = 0;
        while (!queue.isEmpty()) {
            level++;
            int len = queue.size();
            int levelMaxValue = Integer.MIN_VALUE;
            for (int i = 0; i < len; i++) {
                TreeNode treeNode = queue.poll();
                levelMaxValue = Math.max(levelMaxValue, treeNode.val);
                TreeNode childLeft = treeNode.left;
                TreeNode childRight = treeNode.right;
                if (childLeft != null) {
                    queue.add(childLeft);
                }
                if (childRight != null) {
                    queue.add(childRight);
                }
            }
            result.add(levelMaxValue);
        }
        return result;
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

}
