package goanimate;

import java.util.Scanner;

public class Test04 {

    public static void main(String[] args) {
        // Scanner in = new Scanner(System.in);
        // int cnt = in.nextInt();
        // for (int i = 0; i < cnt; i++) {
        // int lenOfAry = in.nextInt();
        // int[] ary = new int[lenOfAry];
        // for (int j = 0; j < lenOfAry; j++) {
        // ary[j] = in.nextInt();
        // }
        // print(lenOfAry, ary);
        // }

        print(1, new int[] { 1, 100 });
    }

    public static void print(int n, int[] ary) {
        int temp = 0;
        for (int item : ary) {
            if (item == 1 || temp == 1) {
                System.out.println("YES");
                return;
            }
            if (temp == 0) {
                temp = item;
                continue;
            }
            temp = gcd(temp, item);
        }
        if (temp == 1) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }
    }

    public static int gcd(int m, int n) {
        return n == 0 ? m : gcd(n, m % n);
    }
}
