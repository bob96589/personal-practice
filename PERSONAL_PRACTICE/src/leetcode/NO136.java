package leetcode;

public class NO136 {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

    // @@@[bit manipulation] find single
    public static int singleNumber(int[] nums) {
        int result = 0;
        for (int num : nums) {
            result ^= num;
        }
        return result;
    }
}
